﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Users
/// </summary>
public static class Users
{
    public static SqlConnection db = new SqlConnection("Server=198.143.164.5;Database=UpTacks;User Id=uptacks;Password=printery");

    public static bool validateLogin(string  email, string  password)
    {
        db.Close();
        //cuz mohammed needs to complete the regex validation! :)
        if (email == "" || password == "")
        {
            return false;
        }
        bool valid = false;
        db.Open();
       // SqlCommand query = new SqlCommand("select * from [users] where EMAIL = '" + email + "' and password = '" + password + "'", db);
        SqlCommand query = new SqlCommand("GET_USER_EMAIL_PASSWORD", db);
        query.CommandType = System.Data.CommandType.StoredProcedure;
        query.Parameters.AddWithValue("email", email);
        query.Parameters.AddWithValue("password", password);
        if (query.ExecuteReader().HasRows)
        {
            //email and password exist
            valid = true;
        }
        db.Close();
        return valid;
    }

    public static bool validateFB(string fbid)
    {
        db.Close();
        //cuz mohammed needs to complete the regex validation! :)
        if (fbid == "")
        {
            return false;
        }
        bool valid = false;
        db.Open();
         SqlCommand query = new SqlCommand("select * from [users] where fbid= '" + fbid+ "'", db);
 //       SqlCommand query = new SqlCommand("GET_USER_EMAIL_PASSWORD", db);
   //     query.CommandType = System.Data.CommandType.StoredProcedure;
    //    query.Parameters.AddWithValue("email", email);
     //   query.Parameters.AddWithValue("password", password);
        if (query.ExecuteReader().HasRows)
        {
            //email and password exist
            valid = true;
        }
        db.Close();
        return valid;
    }

    public static bool registerNewUser(string  email, string  firstName, string  lastName, string  password)
    {
        db.Close();
        //cuz mohammed needs to complete the regex validation! :)
        if (email == "" || password == "")
        {
            return false;
        }
        bool registered = false;
        db.Open();
        //SqlCommand query = new SqlCommand("select * from [users] where EMAIL = '" + email + "'", db);
        SqlCommand query = new SqlCommand("get_user_by_email", db);
        query.CommandType = System.Data.CommandType.StoredProcedure;
        query.Parameters.AddWithValue("email", email);
        if (!query.ExecuteReader().HasRows)
        {
            //no email exists. User can get registered.
            db.Close();
            db.Open();
            //SqlCommand insertQuery = new SqlCommand("insert into [Users] values('" + firstName + "','" + lastName + "','" + email + "','" + password + "',NULL)", db);
            SqlCommand insertQuery = new SqlCommand("users_insert", db);
            insertQuery.CommandType = System.Data.CommandType.StoredProcedure;
            insertQuery.Parameters.AddWithValue("first_name", firstName);
            insertQuery.Parameters.AddWithValue("last_name", lastName);
            insertQuery.Parameters.AddWithValue("email", email);
            insertQuery.Parameters.AddWithValue("password", password);
            insertQuery.ExecuteNonQuery();
            registered = true;
            db.Close();
        }
        db.Close();
        return registered;
    }

    public static bool registerNewFBUser(string email, string firstName, string lastName, string password, string fbid)
    {
        //cuz mohammed needs to complete the regex validation! :)
        if (email == "" || password == "")
        {
            return false;
        }
        bool registered = false;
        db.Close();
        db.Open();
        SqlCommand query = new SqlCommand("select * from [users] where EMAIL = '" + email + "'", db);
      //  SqlCommand query = new SqlCommand("GET_USER_BY_EMAIL", db);
       // query.CommandType = System.Data.CommandType.StoredProcedure;
        //query.Parameters.AddWithValue("email", email);
        if (!query.ExecuteReader().HasRows)
        {
            db.Close();
            db.Open();
            //no email exists. User can get registered.
            SqlCommand insertQuery = new SqlCommand("insert into Users (FIRST_NAME,LAST_NAME,EMAIL,PASSWORD,fbid,PROFILE_PICTURE) values('" + firstName + "','" + lastName + "','" + email + "','" + password + "','" + fbid + "',NULL)", db);
//            SqlCommand insertQuery = new SqlCommand("users_insert", db);
 //           insertQuery.CommandType = System.Data.CommandType.StoredProcedure;
  //          insertQuery.Parameters.AddWithValue("first_name", firstName);
   //         insertQuery.Parameters.AddWithValue("last_name", lastName);
    //        insertQuery.Parameters.AddWithValue("email", email);
     //       insertQuery.Parameters.AddWithValue("password", password);
      //      insertQuery.ExecuteNonQuery();
            SqlDataReader results = insertQuery.ExecuteReader();
            registered = true;
        }
        db.Close();
        return registered;
    }

    public static User getUserByEmail(string  email)
    {
        db.Close();
        db.Open();
        //SqlCommand query = new SqlCommand("select * from [users] where email = '" + email + "'", db);
        SqlCommand query = new SqlCommand("GET_USER_BY_EMAIL", db);
        query.CommandType = System.Data.CommandType.StoredProcedure;
        query.Parameters.AddWithValue("email", email);
        SqlDataReader results = query.ExecuteReader();
        User user;
        if (results.Read())
        {
            user = new User(results["user_id"].ToString(), 
                results["first_name"].ToString(), 
                results["last_name"].ToString(), 
                results["email"].ToString());
        }
        else
        {
            return null;
        }
        db.Close();
        return user;
    }

    public static User geUserById(string  id)
    {
        db.Close();
        db.Open();
       // SqlCommand query = new SqlCommand("select * from [users] where user_id = '" + id + "'", db);
        SqlCommand query = new SqlCommand("GET_USER_BY_ID", db);
        query.CommandType = System.Data.CommandType.StoredProcedure;
        query.Parameters.AddWithValue("id", id);
        SqlDataReader results = query.ExecuteReader();
        User user;
        if (results.Read())
        {
            user = new User(results["user_id"].ToString(),
                results["first_name"].ToString(),
                results["last_name"].ToString(),
                results["EMAIL"].ToString());
                
        }
        else
        {
            return null;
        }
        db.Close();
        return user;
    }

    public static User getUserByID(string id)
    {
        db.Close();
        db.Open();
        //SqlCommand query = new SqlCommand("select * from [users] where user_id = '" + id + "'", db);
        SqlCommand query = new SqlCommand("GET_USER_BY_ID", db);
        query.CommandType = System.Data.CommandType.StoredProcedure;
        query.Parameters.AddWithValue("id", id);
        SqlDataReader results = query.ExecuteReader();
        User user;
        if (results.Read())
        {
            user = new User(results["user_id"].ToString(),
                results["first_name"].ToString(),
                results["last_name"].ToString(),
                results["email"].ToString());
        }
        else
        {
            return null;
        }
        db.Close();
        return user;
    }
    public static User getUserByFBID(string id)
    {
        db.Close();

        db.Open();
        SqlCommand query = new SqlCommand("select * from [users] where fbid = '" + id + "'", db);
        SqlDataReader results = query.ExecuteReader();
        User user;
        if (results.Read())
        {
            user = new User(results["user_id"].ToString(),
                results["first_name"].ToString(),
                results["last_name"].ToString(),
                results["email"].ToString());
        }
        else
        {
            return null;
        }
        db.Close();
        return user;
    }

    public static string getUserPassword(string id)
    {
        string password = null;

        db.Close();

        db.Open();
        //SqlCommand query = new SqlCommand("GET_USER_BY_ID", db);
        //query.CommandType = System.Data.CommandType.StoredProcedure;
        //query.Parameters.AddWithValue("id", id);

        SqlCommand query = new SqlCommand("select * from [users] where user_id = '" + id + "'", db);
        SqlDataReader results = query.ExecuteReader();

        if (results.Read())
        {
            password = results["password"].ToString();
        }
        else
        {
            return null;
        }
        db.Close();
        return password;
    }

    public static void setNewPassword(string id, string newPassword)
    {
        db.Close();

        db.Open();
        SqlCommand query = new SqlCommand("update users set password='" + newPassword + "' where user_id = '" + id + "'", db);
        query.ExecuteReader();

        db.Close();
    }
}