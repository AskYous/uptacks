﻿using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Boards
/// </summary>
public class Tacks
{
    public static SqlConnection db = new SqlConnection("Server=198.143.164.5;Database=UpTacks;User Id=uptacks;Password=printery;MultipleActiveResultSets=true");

    public static bool createNewTack(string userID, string name, string description, string imgURL, string boardID, string websiteurl)
    {
        //cuz mohammed needs to complete the regex validation! :)
        if (name == "" || description == "" || imgURL == "")
        {
            return false;
        }
        bool created = false;
        db.Close();
        db.Open();
       /* SqlCommand query = new SqlCommand("Select * from tack where user_id = " + userID
            + " and board_id = '" + boardID + "'"
            + " and tack_title = '" + name + "'", db);*/
        SqlCommand query = new SqlCommand("GET_TACKS", db);
        query.CommandType = System.Data.CommandType.StoredProcedure;
        query.Parameters.AddWithValue("user_id", userID);
        query.Parameters.AddWithValue("tack_title", name);
        query.Parameters.AddWithValue("board_id", boardID);
        if (!query.ExecuteReader().HasRows)
        {
            //no tack exists. Tack can get created.
            db.Close();
            db.Open();
            SqlCommand insertQuery = new SqlCommand("create_new_tack", db);
            insertQuery.CommandType = System.Data.CommandType.StoredProcedure;
            insertQuery.Parameters.AddWithValue("BOARD_ID", boardID);
            insertQuery.Parameters.AddWithValue("user_id", userID);
            insertQuery.Parameters.AddWithValue("title", name);
            insertQuery.Parameters.AddWithValue("description", description);
            insertQuery.Parameters.AddWithValue("imageurl", imgURL);
            insertQuery.Parameters.AddWithValue("WebsiteURL", websiteurl);
            insertQuery.ExecuteNonQuery();
            created = true;
        }
        db.Close();
        return created;
    }

    public static bool deleteTackById(string tackID)
    {
        bool deleted = false;
        db.Close();
        db.Open();
        //check if tack exist
        SqlCommand query = new SqlCommand("Select * from tack where tack_id = '" + tackID + "'", db);
        SqlDataReader results = query.ExecuteReader();

        if (results.HasRows)
        {
            // delete the tack
            //SqlCommand deleteQuery = new SqlCommand("Delete from tack where tack_id = '" + tackID + "'", db);
            SqlCommand deleteQuery = new SqlCommand("DELETE_FROM_TACK_BY_TACK_ID", db);
            deleteQuery.CommandType = System.Data.CommandType.StoredProcedure;
            deleteQuery.Parameters.AddWithValue("tack_id", tackID);
            deleteQuery.ExecuteNonQuery();
            deleted = true;
        }
        db.Close();
        results.Close();
        return deleted;
    }

    public static Tack getTackById(string tackId)
    {
        db.Close();
        db.Open();
        //SqlCommand query = new SqlCommand("Select * from tack where tack_id = '" + tackId + "'", db);
        SqlCommand query = new SqlCommand("get_tacks_by_id", db);
        query.CommandType = System.Data.CommandType.StoredProcedure;
        query.Parameters.AddWithValue("tack_id", tackId);
        SqlDataReader results = query.ExecuteReader();
        Tack tack = null;

        if (results.Read())
        {
            string id = results["tack_id"].ToString();
            User owner = Users.getUserByID(results["user_id"].ToString());
            Board board = Boards.getBoardById(results["board_id"].ToString());
            string title = results["tack_title"].ToString();
            string description = results["tack_desc"].ToString();
            string imgURL = results["image_url"].ToString();    
            string webURL = results["website_URL"].ToString();

            tack = new Tack(id, owner, title, description, imgURL, board, webURL);
        }
        results.Close();
        return tack;
    }

    public static List<Tack> getNewestTacks(int amount)
    {
        List<Tack> newestTacks = new List<Tack>();
        List<string> tacksIDs = new List<string>();

        db.Close();
        db.Open();

        SqlCommand insertQuery = new SqlCommand("get_newest_tacks_2", db);
        insertQuery.CommandType = System.Data.CommandType.StoredProcedure;
       // insertQuery.CommandType = System.Data.CommandType.StoredProcedure;
        insertQuery.ExecuteNonQuery();
        SqlDataReader results = insertQuery.ExecuteReader();
        int i = 0;
        while (results.Read() && i < amount)
        {
            tacksIDs.Add(results["tack_ID"].ToString());
            i++;
        }

        foreach (string ID in tacksIDs)
        {
            newestTacks.Add(Tacks.getTackById(ID));
        }
        return newestTacks;
    }
    public static List<Tack> searchTacks(string searchterm)
    {
        List<Tack> foundTacks = new List<Tack>();
        List<string> tacksIDs = new List<string>();

        db.Close();
        db.Open();

        //SqlCommand searchQuery = new SqlCommand("Select * from tack where tack_title Like '%" + searchterm + "%' OR tack_desc Like '%" + searchterm + "%' ORDER BY DATE_SUBMITTED_WITH_TIME  DESC", db);
        SqlCommand searchQuery = new SqlCommand("SELECT * FROM TACK T LEFT JOIN BOARD B ON T.BOARD_ID = B.BOARD_ID WHERE B.PRIVACY = 1 AND (tack_title Like '%" + searchterm + "%' OR tack_desc Like '%" + searchterm + "%') ORDER BY DATE_SUBMITTED_WITH_TIME  DESC", db);
        // insertQuery.CommandType = System.Data.CommandType.StoredProcedure;
        searchQuery.ExecuteNonQuery();
        SqlDataReader results = searchQuery.ExecuteReader();
        while (results.Read())
        {
            tacksIDs.Add(results["tack_ID"].ToString());
        }

        foreach (string ID in tacksIDs)
        {
            foundTacks.Add(Tacks.getTackById(ID));
        }
        db.Close();
        return foundTacks;
    }
}