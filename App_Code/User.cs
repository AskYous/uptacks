﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for User
/// </summary>
public class User
{
    public static SqlConnection db = new SqlConnection("Server=198.143.164.5;Database=UpTacks;User Id=uptacks;Password=printery");
    private string id;
    private string fname;
    private string lname;
    private string email;

    public User(string id, string fname, string lname, string email)
    {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.email = email;
    }

    public List<Board> getBoards()
    {
        List<Board> list = new List<Board>();
        db.Close();
        db.Open();
        Board temp;
        User user;
        SqlCommand query = new SqlCommand("get_board_by_owner",db );
        query.CommandType = System.Data.CommandType.StoredProcedure;
        query.Parameters.AddWithValue("user_id", id);
        SqlDataReader results = query.ExecuteReader();
        
        while(results.Read()){
            bool isPrivate = false;
            if(results["privacy"].ToString() == "0")
            {
                isPrivate = true;
            }

                   temp = new Board(results["board_id"].ToString(),Users.getUserByID(results["user_id"].ToString()), 
                results["board_title"].ToString(), 
                results["board_desc"].ToString(), 
                isPrivate,
                results["img_url"].ToString());
                list.Add(temp);
        }
            db.Close();
            return list;
    }

    public bool addBoard(Board b)
    {
        //not yet implemented
        return false;
    }

    public string getID()
    {
        return id;
    }

    public string getFullName()
    {
        return fname + " " + lname;
    }


    public List<Tack> getFavorateTacks(int numOfTacks)
    {
        db.Close();
        db.Open();
        SqlCommand sqlcmd = new SqlCommand("Select * from favorites where user_id = " + id, db);
        SqlDataReader results = sqlcmd.ExecuteReader();
        List<Tack> favTacks = new List<Tack>();
        List<string> tackIDs = new List<string>();
        int i = 0;
        while (results.Read() && i < numOfTacks)
        {
            tackIDs.Add(results["tack_id"].ToString());
            i++;
        }

        foreach (string tackID in tackIDs)
        {
            favTacks.Add(Tacks.getTackById(tackID));
        }
        db.Close();
        return favTacks;
    }

    public List<Tack> getTacksFromFollowedBoards(int numOfTacks)
    {
        db.Close();
        db.Open();
        SqlCommand sqlcmd = new SqlCommand("Select following_id as board_id from following where follower_id = " + id, db);
        SqlDataReader results = sqlcmd.ExecuteReader();
        List<string> boardIDs = new List<string>();
        while (results.Read())
        {
            boardIDs.Add(results["board_id"].ToString());
        }
        db.Close();

        List<Tack> tacks = new List<Tack>();
        foreach (string boardID in boardIDs)
        {
            Board b = Boards.getBoardById(boardID);
            foreach (Tack t in b.getTacks())
            {
                tacks.Add(t);
            }
        }

        return tacks;
    }

    public void changePassword(string currentPassword, string newPassword)
    {
        if (Users.getUserPassword(id).Equals(currentPassword))
        {
            Users.setNewPassword(id, newPassword);
        }
    }
}