﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Board
/// </summary>
public class Board
{
    public static SqlConnection db = new SqlConnection("Server=198.143.164.5;Database=UpTacks;User Id=uptacks;Password=printery");
    string id;
    private string title;
    private User owner;
    private string description;
    private bool isPrivate;
    private string imgURL;

    public Board(string id, User owner, string title, string description, bool isPrivate, string imgURL)
    {
        this.id = id;
        this.owner = owner;
        this.title = title;
        this.description = description;
        this.isPrivate = isPrivate;
        this.imgURL = imgURL;
    }

    public List<Tack> getTacks()
    {
        db.Close();
        db.Open();
       // SqlCommand query = new SqlCommand("select * from tack where board_id = '" + id + "'", db);
        SqlCommand query = new SqlCommand("GET_TACK_BY_BOARD_ID", db);
        query.CommandType = System.Data.CommandType.StoredProcedure;
        query.Parameters.AddWithValue("BOARD_ID", id);
        SqlDataReader results = query.ExecuteReader();
        List<Tack> tacks = new List<Tack>();
        Tack tack;
        while (results.Read())
        {
            tack = new Tack(results["tack_id"].ToString(),
                    Users.getUserByID(results["User_ID"].ToString()),
                    results["TACK_TITLE"].ToString(),
                    results["tack_desc"].ToString(),
                    results["image_url"].ToString(),
                    Boards.getBoardById(results["board_id"].ToString()),
                    results["website_url"].ToString()
                );
            tacks.Add(tack);
        }
        return tacks;
    }

    public bool addTack(Tack tack)
    {
        //not yet implemented
        return false;
    }

    public string getID()
    {
        return id;
    }
    public string getOwner()
    {
        return owner.getID();
    }

    public string getTitle()
    {
        return title;

    }
    public string getDescription()
    {
        return description;
    }

    public string getImgURL()
    {
        return imgURL;
    }

    public bool getPrivacy()
    {
        return isPrivate;
    }

    public User getUserOwner(){
        return owner;
    }

    public void setTitle(string newTitle)
    {
        newTitle = newTitle.Replace("'", "");
        this.title = newTitle;
    }

    public void setDescription(string newDescription)
    {
        newDescription = newDescription.Replace("'", "");
        this.description = newDescription;
    }

    public void setImgURL(string newImgURL)
    {
        newImgURL = newImgURL.Replace("'", "");
        this.imgURL = newImgURL;
    }

    public void setPrivate(bool newIsPrivate)
    {
        this.isPrivate = newIsPrivate;
    }

    public void commitChanges()
    {
        SqlConnection db = new SqlConnection("Server=198.143.164.5;Database=UpTacks;User Id=uptacks;Password=printery;MultipleActiveResultSets=true");
        db.Open();
        string privacy = "1";
        if (isPrivate)
        {
            privacy = "0";
        }
        string sqlstatement = 
            "Update board " +
            "set board_title = '" + title + 
            "', board_desc = '" + description + 
            "', privacy = '" + privacy + 
            "', img_url='" + imgURL + 
        "' where board_id = '" + id + "'";

        SqlCommand query = new SqlCommand(sqlstatement, db);
        SqlDataReader results = query.ExecuteReader();
        db.Close();
    }

    public bool follow(User user)
    {
        db.Close();
        db.Open();
        string uid = user.getID();
        bool followed;
        //Insert the like association
        //Stored procedures needed!!!
        SqlCommand query = new SqlCommand("Select * from following where follower_id = '" + uid + "' AND  following_id = '" + id + "'", db);
        //SqlCommand query = new SqlCommand("GET_LIKES", db);
        //query.CommandType = System.Data.CommandType.StoredProcedure;
        //query.Parameters.AddWithValue("user_id", uid);
        //query.Parameters.AddWithValue("tack_id", id);
        SqlDataReader results = query.ExecuteReader();
        if (!results.HasRows) // If it's not liked
        {
            db.Close();
            db.Open();
            query = new SqlCommand("INSERT INTO following(follower_id, following_id) VALUES('" + uid + "','" + id + "')", db);
            //query = new SqlCommand("INSERT_LIKES", db);
            //query.CommandType = System.Data.CommandType.StoredProcedure;
            //query.Parameters.AddWithValue("user_id", uid);
            //query.Parameters.AddWithValue("tack_id", id);
            //query.ExecuteNonQuery();
            results = query.ExecuteReader();
            followed = true;
        }
        else //UNFAVORITE
        {
            query = new SqlCommand("DELETE FROM following WHERE follower_id = '" + uid + "' AND following_id = '" + id + "')", db);
            //query = new SqlCommand("DELETE_LIKES", db);
            //query.CommandType = System.Data.CommandType.StoredProcedure;
            //query.Parameters.AddWithValue("user_id", uid);
            //query.Parameters.AddWithValue("tack_id", id);
            //query.ExecuteNonQuery();
            results = query.ExecuteReader();
            followed = false;
        }
        return followed;
    }

    public bool isFollowed(User user)
    {
        db.Close();
        db.Open();
        string uid = user.getID();
        bool followed;
        SqlCommand query = new SqlCommand("Select * from following where follower_ID = '" + uid + "' AND  Following_id = '" + id + "'", db);
        //SqlCommand query = new SqlCommand("GET_LIKES", db);
        //query.CommandType = System.Data.CommandType.StoredProcedure;
        //query.Parameters.AddWithValue("follower_id", uid);
        //query.Parameters.AddWithValue("following_id", id);
        SqlDataReader results = query.ExecuteReader();
        if (results.HasRows) { followed = true; }
        else { followed = false; }
        return followed;
    }

}