﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Boards
/// </summary>
public class Boards
{
    public static SqlConnection db = new SqlConnection("Server=198.143.164.5;Database=UpTacks;User Id=uptacks;Password=printery;MultipleActiveResultSets=true");

    public Boards()
    {
        db.Close();
    }

    public static bool createNewBoard(string userID, string name, string description, string privacy, string imgURL)
    {
        bool created = false;
        db.Close();
        db.Open();
        //SqlCommand query = new SqlCommand("Select * from board where user_id = " + userID + " and board_title = '" + name + "'", db);
        SqlCommand query = new SqlCommand("GET_BOARD_BY_OWNER_Title", db);
        query.CommandType = System.Data.CommandType.StoredProcedure;
        query.Parameters.AddWithValue("user_id", userID);
        query.Parameters.AddWithValue("board_title", name);
      
        if (!query.ExecuteReader().HasRows)
        {
            //no board exists. Board can get created.
            db.Close();
            db.Open();
            SqlCommand insertQuery = new SqlCommand("create_new_board", db);
            insertQuery.CommandType = System.Data.CommandType.StoredProcedure;
            insertQuery.Parameters.AddWithValue("user_id", userID);
            insertQuery.Parameters.AddWithValue("BOARD_TITLE", name);
            insertQuery.Parameters.AddWithValue("board_desc", description);
            insertQuery.Parameters.AddWithValue("privacy", privacy);
            insertQuery.Parameters.AddWithValue("img_url", imgURL);
            insertQuery.ExecuteNonQuery();
            created = true;
        }
        db.Close();
        return created;
    }

    public static Board getBoardByOwnerAndTitle(User owner, string boardTitle)
    {
        db.Open();
        //SqlCommand query = new SqlCommand("Select * from board where user_id = '" + owner.getID() + "' and board_title = '" + boardTitle + "'", db);
        SqlCommand query = new SqlCommand("GET_BOARD_BY_OWNER_Title", db);
        query.CommandType = System.Data.CommandType.StoredProcedure;
        query.Parameters.AddWithValue("user_id", owner.getID());
        query.Parameters.AddWithValue("board_title", boardTitle);
        SqlDataReader results = query.ExecuteReader();
        Board board;
        if (results.Read())
        {
            bool isPrivate = true;
            if (results["privacy"].ToString().Equals("1"))
            {
                isPrivate = false;
            }

            board = new Board(results["board_id"].ToString(),
                owner,
                results["board_title"].ToString(),
                results["board_desc"].ToString(),
                isPrivate,
                results["img_URL"].ToString()
                );
        }
        else
        {
            board = null;
        }
        db.Close();
        results.Close();
        return board;
    }

    public static bool deleteBoardById(string boardID)
    {
        bool deleted = false;
        db.Close();
        db.Open();
        //check if user actually owns that board
        //SqlCommand query = new SqlCommand("Select * from board where board_id = '" + boardID + "'", db);
        SqlCommand query = new SqlCommand("GET_BOARD_BY_BOARD_ID", db);
        query.CommandType = System.Data.CommandType.StoredProcedure;
        query.Parameters.AddWithValue("BOARD_ID", boardID);
        SqlDataReader results = query.ExecuteReader();

        if (results.HasRows)
        {
            //user owns that board. Safe to delete board.
          //  SqlCommand deleteQuery = new SqlCommand("Delete from board where board_id = '" + boardID + "'", db);
            SqlCommand deleteQuery = new SqlCommand("DELETE_FROM_BOARD_BY_ID", db);
            deleteQuery.CommandType = System.Data.CommandType.StoredProcedure;
            deleteQuery.Parameters.AddWithValue("board_id", boardID);
            deleteQuery.ExecuteNonQuery();
            deleted = true;
        }
        db.Close();
        if(deleted)
        {
            db.Open();
            SqlCommand deleteTacksQuery = new SqlCommand("Delete from Tack where board_id = '" + boardID + "'", db);
            deleteTacksQuery.ExecuteNonQuery();
        }

        results.Close();
        return deleted;
    }

    public static Board getBoardById(string boardId)
    {
        db.Close();
        db.Open();
        SqlCommand query = new SqlCommand("Select * from board where board_id = '" + boardId + "'", db);
        SqlDataReader results = query.ExecuteReader();
        Board board = null;

        if (results.Read())
        {
            String id = results["board_id"].ToString();
            User owner = Users.getUserByID(results["user_id"].ToString());
            String title = results["board_title"].ToString();
            String description = results["board_desc"].ToString();
            bool isPrivate = false;
            if(results["privacy"].ToString().Equals("0")){
                isPrivate = true;
            }
            string imgURL = results["img_url"].ToString();

            board = new Board(id, owner, title, description, isPrivate, imgURL);
        }
        results.Close();
        return board;
    }

}