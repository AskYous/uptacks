﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Comment
/// </summary>
public static class Comment
{
    public static SqlConnection db = new SqlConnection("Server=198.143.164.5;Database=UpTacks;User Id=uptacks;Password=printery");
    private static string id;
    private static User owner;
    private static Tacks tacks;
    private static string tackID;
    private static string userID;
    private static string commentId;
    private static string setContent;
    private static string getContent;
    private static int getContentCount = 0;
    private static string comment;
    private static string commentName;

    //getTackID() needs to be implemented from Tacks class

    public static void insertComment(String commentUserId, String commentTackId, String commentTextValue, String sessionNameValue)
    {
        db.Close();
        db.Open();
        commentTextValue = commentTextValue.Replace("'", "");
        commentTextValue = commentTextValue.Replace(";", "");
        commentTextValue = commentTextValue.Replace(")", "");
        commentTextValue = commentTextValue.Replace("(", "");
        commentTextValue = commentTextValue.Replace(",", "");
        SqlCommand insertQuery = new SqlCommand("insert into COMMENT values('" + commentUserId + "','" + commentTackId + "','" + commentTextValue + "','" + sessionNameValue +"')", db);
        insertQuery.ExecuteNonQuery();
        db.Close();
    }



    public static List<String> getComment(String commentTackId)
    {
        List<String> addComment = new List<String>();
        addComment.Clear();
        db.Close();
        db.Open();
        SqlCommand query = new SqlCommand("select * from [Comment] where TACK_ID = '" + commentTackId + "'", db);
        SqlDataReader results = query.ExecuteReader();
        while (results.Read())
        {
            comment = results["CONTENT"].ToString();
            commentName = results["NAME"].ToString();
            addComment.Add("<b>" + commentName + ":</b><br/>&emsp;" + comment);

        }

        return addComment;
    }


    public static String getCommentID()
    {
        db.Close();
        db.Open();
        SqlCommand query = new SqlCommand("select ID from [Comment] where USER_ID = '" + userID + "' and TACK_ID = '" + tackID + "'", db);
        SqlDataReader results = query.ExecuteReader();
        if (results.Read())
        {
            String id = results["ID"].ToString();
            commentId = id;
        }

        results.Close();
        db.Close();
        return commentId;
    }



}