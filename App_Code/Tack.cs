﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

/// <summary>
/// Summary description for Tack
/// </summary>
public class Tack
{
    public static SqlConnection db = new SqlConnection("Server=198.143.164.5;Database=UpTacks;User Id=uptacks;Password=printery");
    string id;
    private string title;
    private User owner;
    private Board board;
    private string description;
    private string imgURL;
    private string websiteURL;

	public Tack(string id, User owner, string title,  string description, string imgURL, Board board, string websiteURL)
	{
        this.id = id;
        this.board = board;
        this.owner = owner;
        this.title = title;
        this.description = description;
        this.imgURL = imgURL;
        this.websiteURL = websiteURL;
	}

    public string parseYouTubeURL(string YouTubeURL)
    {
        string embedURL = "www.youtube.com/embed/";
        string videoID = YouTubeURL.Substring(Math.Max(0, YouTubeURL.Length - 11));
        return (embedURL + videoID);
    }

    public string getYoutubeID()
    {
        Regex YoutubeVideoRegex = new Regex(@"(?:https?\:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v\=))([\w-]{10,12})(?:$|\&|\?\#).*", RegexOptions.IgnoreCase);

        Match youtubeMatch = YoutubeVideoRegex.Match(getWebsiteURL());

        string id = string.Empty;

        if (youtubeMatch.Success)
            id = youtubeMatch.Groups[1].Value;

        return id;
    }

    //NOTHING IN THIS METHODE IS TESTED YET!! I JUT WROTE IT AS I THINK IT WOULD WORK!!
    public bool favorite(User user, Tack tack)
    {
        db.Close();
        db.Open();
        string uid = user.getID();
        string id = tack.getID();
        bool favorited;
        //Insert the favorite association
        //Stored procedure needed!!!
        SqlCommand query = new SqlCommand("Select * from favorites where user_id = '" + uid + "' AND  tack_id = '" + id + "'", db);
  //      SqlCommand query = new SqlCommand("GET_FROM_FAVORITES", db);
       
//        SqlCommand query = new SqlCommand(sqlquery, db);
        //SqlDataReader results = query.ExecuteReader();
        //db.Close();
        /*query.CommandType = System.Data.CommandType.StoredProcedure;
        query.Parameters.AddWithValue("user_id", uid);
        query.Parameters.AddWithValue("tack_id", id);*/
        SqlDataReader results = query.ExecuteReader();
        if (!results.HasRows) // If it's not favorited
        {
            db.Close();
            db.Open();
            query = new SqlCommand("INSERT INTO favorites(user_id, tack_id) VALUES('" + uid + "','" + id + "')", db);
            //            query = new SqlCommand("INSERT_INTO_FAVORITES", db);
            //          query.CommandType = System.Data.CommandType.StoredProcedure;
            //         query.Parameters.AddWithValue("user_id", uid);
            //        query.Parameters.AddWithValue("tack_id", id);
            //       query.ExecuteNonQuery();
            results = query.ExecuteReader();
            favorited = true;
        }
        else
        {
            favorited = false;
        }

        return favorited;
    }

    public string getBoardId(){
        return board.getID();
    }

    public string getWebsiteURL()
    {
        return websiteURL;
    }

    public User getOwner()
    {
        return owner;
    }

    public bool isFavorited(User user)
    {
        db.Close();
        db.Open();
        string uid = user.getID();
        bool favorited;
        SqlCommand query = new SqlCommand("Select * from favorites where user_id = '" + uid + "' AND  tack_id = '" + id + "'", db);
        //SqlCommand query = new SqlCommand("DELETE_FROM_FAVORITES", db);
        //query.CommandType = System.Data.CommandType.StoredProcedure;
        //query.Parameters.AddWithValue("user_id", uid);
        //query.Parameters.AddWithValue("tack_id", id);
        query.ExecuteNonQuery();
        SqlDataReader results = query.ExecuteReader();
        if (results.HasRows) { favorited = true; }
        else { favorited = false; }
        db.Close();

        return favorited;
    }

    //WHERE IS THE LIKE TABLE!!
    public bool like(User user)
    {
        db.Close();
        db.Open();
        string uid = user.getID();
        bool liked;
        //Insert the like association
        //Stored procedures needed!!!
       // SqlCommand query = new SqlCommand("Select * from likes where user_id = '" + uid + "' AND  tack_id = '" + id + "'", db);
        SqlCommand query = new SqlCommand("GET_LIKES", db);
        query.CommandType = System.Data.CommandType.StoredProcedure;
        query.Parameters.AddWithValue("user_id", uid);
        query.Parameters.AddWithValue("tack_id", id);
        SqlDataReader results = query.ExecuteReader();
        if (!results.HasRows) // If it's not liked
        {
            //query = new SqlCommand("INSERT INTO likes(user_id, tack_id) VALUES('" + uid + "','" + id + "')", db);
            query = new SqlCommand("INSERT_LIKES", db);
            query.CommandType = System.Data.CommandType.StoredProcedure;
            query.Parameters.AddWithValue("user_id", uid);
            query.Parameters.AddWithValue("tack_id", id);
            query.ExecuteNonQuery();
            results = query.ExecuteReader();
            liked = true;
        }
        else //UNFAVORITE
        {
            //query = new SqlCommand("DELETE FROM likes WHERE user_id = '" + uid + "' AND tack_id = '" + id + "')", db);
            query = new SqlCommand("DELETE_LIKES", db);
            query.CommandType = System.Data.CommandType.StoredProcedure;
            query.Parameters.AddWithValue("user_id", uid);
            query.Parameters.AddWithValue("tack_id", id);
            query.ExecuteNonQuery();
            results = query.ExecuteReader();
            liked = false;
        }
        return liked;
    }

    public bool isLiked(User user)
    {
        db.Close();
        db.Open();
        string uid = user.getID();
        bool liked;
        //SqlCommand query = new SqlCommand("Select * from likes where user_id = '" + uid + "' AND  tack_id = '" + id + "'", db);
        SqlCommand query = new SqlCommand("GET_LIKES", db);
        query.CommandType = System.Data.CommandType.StoredProcedure;
        query.Parameters.AddWithValue("user_id", uid);
        query.Parameters.AddWithValue("tack_id", id);
        SqlDataReader results = query.ExecuteReader();
        if (results.HasRows) { liked = true; }
        else { liked = false; }
        return liked;
    }

    public string getID()
    {
        return id;
    }

    public string getImgURL()
    {
        return imgURL;
    }

    public string getTitle()
    {
        return title;
    }

    public string getDescription()
    {
        return description;
    }

    public void setTitle(string newTackTitle)
    {
        newTackTitle = newTackTitle.Replace("'", "");
        this.title = newTackTitle;
    }

    public void setDescription(string newTackDescription)
    {
        newTackDescription = newTackDescription.Replace("'", "");
        this.description = newTackDescription;
    }

    public void setImgURL(string newTackImgURL)
    {
        newTackImgURL = newTackImgURL.Replace("'", "");
        this.imgURL = newTackImgURL;
    }

    public void commitChanges()
    {
        SqlConnection db = new SqlConnection("Server=198.143.164.5;Database=UpTacks;User Id=uptacks;Password=printery;MultipleActiveResultSets=true");
        db.Open();
        string sqlstatement =
            "Update tack " +
            "set tack_title = '" + title +
            "', tack_desc = '" + description +
            "', image_url='" + imgURL +
        "' where tack_id = '" + id + "'";

        SqlCommand query = new SqlCommand(sqlstatement, db);
        SqlDataReader results = query.ExecuteReader();
        db.Close();
    }
}