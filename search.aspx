﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="search.aspx.cs" Inherits="search" %>

<%@ Register Src="~/Navbar.ascx" TagPrefix="uc1" TagName="Navbar" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Uptacks - <% Response.Write(Request.QueryString["search"]); %></title>
    <link href="css/menu-style.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap-responsive.css" rel="stylesheet" type="text/css" />
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <style type="text/css"></style>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/script.js"></script>
    <script>
        $('.dropdown-toggle').dropdown();
    </script>
    <script>
        //function scrollTo(id) {
        //    var eleId = document.getElementById(id);
        //    eleId.scrollIntoView(true);
        //}

        // var shiftWindow = function () { scrollBy(0, -160) };
        //  function load() { if (window.location.hash) shiftWindow(); }
        //window.addEventListener("hashchange", shiftWindow);

    </script>
    <style type="text/css">
        @-webkit-keyframes beat {
            0% {
                -webkit-transform: scale(1) rotate(225deg);
                -webkit-box-shadow: 0 0 40px rgba(213,9,60,1);
            }

            50% {
                -webkit-transform: scale(1.1) rotate(225deg);
                -webkit-box-shadow: 0 0 70px rgba(213,9,60,0.6);
            }

            100% {
                -webkit-transform: scale(1) rotate(225deg);
                -webkit-box-shadow: 0 0 40px rgba(213,9,60,1);
            }
        }

        @-moz-keyframes beat {
            0% {
                -moz-transform: scale(1) rotate(225deg);
                -moz-box-shadow: 0 0 40px rgba(213,9,60,1);
            }

            50% {
                -moz-transform: scale(1.1) rotate(225deg);
                -moz-box-shadow: 0 0 70px rgba(213,9,60,0.6);
            }

            100% {
                -moz-transform: scale(1) rotate(225deg);
                -moz-box-shadow: 0 0 40px rgba(213,9,60,1);
            }
        }

        @keyframes beat {
            0% {
                transform: scale(1) rotate(225deg);
                box-shadow: 0 0 40px #d5093c;
            }

            50% {
                transform: scale(1.1) rotate(225deg);
                box-shadow: 0 0 70px #d5093c;
            }

            100% {
                transform: scale(1) rotate(225deg);
                box-shadow: 0 0 40px #d5093c;
            }
        }

        #chest {
            position: relative;
            width: 50px;
            height: 45px;
            margin: 0 auto;
            margin-top: -35px;
            cursor: pointer;
        }

        .heart {
            position: absolute;
            z-index: 2;
            background: -moz-linear-gradient(-180deg, #F50A45 0%, #d5093c 40%);
            background: -webkit-gradient(linear, right 50%, left 50%, color-stop(0%,#F50A45), color-stop(40%,#d5093c));
            background: -webkit-linear-gradient(-180deg, #F50A45 0%,#d5093c 40%);
            background: linear-gradient(-180deg, #F50A45 0%,#d5093c 40%);
            -webkit-animation: beat 0.7s ease 0s infinite normal;
            -moz-animation: beat 0.7s ease 0s infinite normal;
            animation: beat 0.7s ease 0s infinite normal;
        }

            .heart.center {
                background: -moz-linear-gradient(-45deg, #B80734 0%, #d5093c 40%);
                background: -webkit-gradient(linear, left top, right bottom, color-stop(0%,#B80734), color-stop(40%,#d5093c));
                background: -webkit-linear-gradient(-45deg, #B80734 0%,#d5093c 40%);
                background: linear-gradient(-45deg, #B80734 0%,#d5093c 40%);
            }

            .heart.top {
                z-index: 3;
            }

        .side {
            top: 38px;
            width: 22px;
            height: 22px;
            -moz-border-radius: 220px;
            -webkit-border-radius: 220px;
            border-radius: 220px;
        }

        .center {
            width: 21px;
            height: 21px;
            bottom: -24px;
            left: 14px;
            font-size: 0;
            text-indent: -9999px;
        }

        .left {
            left: 22px;
        }

        .right {
            right: 23px;
        }

        #header.navbar-inner {
            z-index: 5;
            position: fixed;
            width: 100%;
            margin-top: 40px;
        }

        #content {
            padding-top: 100px;
        }

        #blackheart {
            height: 33px;
            width: 39px;
            padding-top: 40px;
        }

        /*#searchTitle{
            font-size:24px;
        }*/
        #searchTitleTerm{
            color:darkgreen;
            font-size: 36px;
            font-weight:bolder;
        }
    </style>
</head>

<body>
    <form id="form1" runat="server">

        <uc1:Navbar runat="server" ID="Navbar" />
            <div id="header" class="navbar-inner">
                <div class="container">
                    <h2 id="searchTitle">Searching for: <span id="searchTitleTerm"><% Response.Write(Request.QueryString["search"]); %></span></h2>
<%--                <ul class="nav nav-pills">
                    <li class="active"><a class="active btn" onclick="document.getElementById('boards').scrollIntoView(); scrollBy(0, -160)">Boards</a></li>
                    <li class="active"><a class="active btn" onclick="document.getElementById('tacks').scrollIntoView(); scrollBy(0, -160)">Tacks</a></li>
                </ul>--%>
                </div>
            </div>       
        <div id="content" class="container" onload="load()">
<%--            <hr id="boards"/>--%>
            <h3>Your Search Returned the Following Tacks</h3>
            <hr />
            <div class="row row-fluid">
                <ul class="tacks">
                    <% List<Tack> foundTacks = Tacks.searchTacks(Request.QueryString["search"]);
                       foreach (Tack t in foundTacks)
                       {
                           Board b =  Boards.getBoardById(t.getBoardId());
                    %>
                    <li class="span3">
                        <div class="thumbnail" id="tack-<% Response.Write(t.getID()); %>">
                            <div class="tack-title"><% Response.Write(t.getTitle()); %></div>
                            <a href="<%= t.getWebsiteURL() %>">
                                <img alt="" src="<%= t.getImgURL() %>" /></a>
                            <div class="caption">
                                <p class="owner" style="float: left;"><%= t.getOwner().getFullName() %></p>
                                <p class="owner" style="float: right;">
                                    <a href="board.aspx?id=<%= b.getID() %>">
                                        <%= b.getTitle() %>
                                    </a>
                                </p>
                                <hr style="visibility: hidden" />
                                <div class="clear"></div>
                                <p style="height: 40px;"><% Response.Write(t.getDescription()); %></p>
                                <br />
                                <hr />
                                <!-- favourite button -->
                                <!-- <asp:Button CssClass="btn pull-center" OnClick="favorite_Click"
                                data-tackID="<%= t.getID() %>" Text="Favorite" runat="server" /> -->


                                <!--comments-->
                                <%List<String> showComments = Comment.getComment(t.getID());%>
                                <div style="width: 247px; height: 150px; overflow: auto; margin: 2px; color: #000000; scrollbar-base-color: #DEBB07;">
                                    <% foreach (String c in showComments)
                                       {                        
                                    %>
                                    <p style="width: 215px; color: white"><% Response.Write(c); %></p>
                                    <%} %>
                                </div>
                                <textarea id="commentText" name="commentText" onfocus="checkComment(<%= t.getID()%>)"></textarea>
                                <!-- favourite -->
                                <div id='chest' onclick='favorite("<%= t.getID() %>","<%= System.Web.HttpUtility.HtmlEncode(t.getTitle()) %>")'>
                                    <% if (t.isFavorited(currentUser)){ %>
                                    <div class="heart left side top"></div>
                                    <div class="heart center">&hearts;</div>
                                    <div class="heart right side"></div>
                                    <% } else { %>
                                    <img id="blackheart" alt="" src="img/theblackheart.png" />
                                    <% } %>
                                </div>


                                <asp:Button ID="Button4" CssClass="btn btn-primary pull-right commentButton" OnClick="submit"
                                    Text="Comment" runat="server" />

                                <!-- Retack Button -->
                                <a class="btn btn-success" onclick='reTack("<%= t.getID() %>","<%= System.Web.HttpUtility.HtmlEncode(t.getTitle()) %>")'>Re-tack</a>


                            </div>
                        </div>
                    </li>
                    <% 
                       }
                    %>
                </ul>
                <asp:HiddenField ID="commentTackId" runat="server" />

            </div>
         </div>

        <!-- Choose board for reTack popup -->
        <div id="modal-editBoard" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>Choose board for Tack: <strong><span id="chooseBoard"></span></strong></h3>
            </div>
            <div class="modal-body">
                <label for="selectBoard">Select Board: </label>
                <select id="boardSelect">
                    <%
                        foreach (Board b in currentUser.getBoards())
                        {
                    %>

                    <option value="<%= b.getID() %>"><%= b.getTitle() %></option>

                    <%
                        }
                    %>
                </select>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <asp:Button ID="retackBtn" CssClass="btn btn-primary" OnClientClick="$('#toBoardID').val($('#boardSelect').val())" runat="server" Text="Save" OnClick="retack_Click" />
            </div>

            <script>
                function reTack(id, title) {
                    $("#retackID").val(id);
                    $("#chooseBoard").html(title);
                    $("#modal-editBoard").modal('show');
                }


            </script>

            <input type="hidden" id="retackID" runat="server" />
            <input type="hidden" id="toBoardID" runat="server" />

        </div>

        <!-- Choose board for favorite popup -->
        <div id="modal-favorite" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>You sure you want to make it Favorite? </h3>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                <asp:Button ID="favbutton" CssClass="btn btn-primary" runat="server" Text="Yes" OnClick="favorite_Click" />
            </div>

            <script>
                function favorite(id, title) {
                    $("#favoriteid").val(id);
                    $("#favoritetitle").html(title);
                    $("#modal-favorite").modal('show');
                }



                function checkComment(dataTackId) {


                    commentLabel = $('.commentButton').attr('dataTackId');


                    $('#commentTackId').val(dataTackId);



                }

            </script>

            <input type="hidden" id="favoriteid" runat="server" />
            <input type="hidden" id="favoritetitle" runat="server" />

        </div>
    </form>
</body>
</html>
