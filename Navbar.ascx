﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Navbar.ascx.cs" Inherits="User_Navbar" %>
<html>
<head>
    <title></title>
    <style>
        .navbar{
            position:fixed;
            width:100%;
            z-index:10;
        }
        input.form-control{
            margin-top:5px;
            margin-bottom:0px;
        }
        .modal-backdrop {
            z-index:9;
        }
    </style>
</head>
<body>
    <div class="navbar navbar-inverse navbar-static-top">
        <div class="navbar-inner">
            <div class="container">
                <% if (Session["userID"] == null)
                   {
                       Response.Redirect("login.aspx");
                   } %>
                <!-- Be sure to leave the brand out there if you want it shown -->
                <a class="brand">Uptacks</a>

                <ul id="navbar" class="nav">
                    <li><a href="dashboard.aspx">Dashboard</a></li>
                    <li><a href="myboards.aspx">My Boards</a></li>
                </ul>

                <ul class="nav pull-right">
                    <li>
                        <!-- USER IMAGE ICON HERE. PLEASE IMPLEMENT -->
                        <asp:TextBox runat="server" ID="searchbox" type="text" class="form-control" placeholder="Search" />
                    </li>
                  <li>
<%--                       <button type="submit" class="btn btn-default">Submit</button>--%>
                      <asp:Button ID="Button1" runat="server" OnClick="search" CssClass="btn btn-default" Text="Search" />
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <% Response.Write(Users.geUserById(Session["userID"].ToString()).getFullName()); %>
                            <b class="caret"></b></a>
                                                <ul class="dropdown-menu">
                            <li><a href="#modal-changePassword" data-toggle="modal" onclick="$('#modal-changePassword').modal('show');">Change Password</a></li>
<!--                            <li><a href="Login.aspx">Sign Out</a></li>-->
                        <li><asp:LinkButton ID="Buttonlogout" runat="server" Text="Log Out " onclick="logout"/></li>

                        </ul>
                    </li>
                </ul>

                <!-- Change Password Popup -->
                <div id="modal-changePassword" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3>Change Password</h3>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="passwordError" runat="server" Visible="false" CssClass="alert alert-error" Text="">.</asp:Label>
                        <asp:TextBox runat="server" ID="currentPassword" textmode="Password" placeholder="Current Password" /><br />
                        <asp:TextBox runat="server" ID="newPassword" textmode="Password" placeholder="New Password" /><br />
                        <asp:TextBox runat="server" ID="newPasswordConfirm" textmode="Password" placeholder="Confirm New Password" /><br />
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="Button2" CssClass="btn btn-danger" runat="server" Text="Change Password" onclick="changePassword"/>
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</body>
</html>

