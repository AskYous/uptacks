﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html5>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <title>Uptacks - Sign in</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content<pages enableEventValidation="true"/>
    <meta name="author" content="">
    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
        body {
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #f5f5f5;
        }

        .form-signin {
            padding: 19px 29px 29px;
            margin: 0 auto 20px;
            background-color: #fff;
            border: 1px solid #e5e5e5;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
        }

            .form-signin .form-signin-heading, .form-signin .checkbox {
                margin-bottom: 10px;
            }

            .form-signin input[type="text"], .form-signin input[type="password"] {
                font-size: 16px;
                height: auto;
                margin-bottom: 15px;
                padding: 7px 9px;
            }

        .container {
            width: 1024px;
        }
    </style>
    <link href="css/menu-style.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap-responsive.css" rel="stylesheet" type="text/css" />
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <style type="text/css"></style>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/script.js"></script>
    <script>
        $('.dropdown-toggle').dropdown();
    </script>

	<script>

	    var i = 0;
	    function test() {
	        if (i == 0) {
	            testfirst();
	            //alert(i);
	            console.log(i);
	        }
	        else {
	            testlogin();
	            //  alert(i);
	            console.log(i);
	        }
	        i++;

	    }

	    function testfirst() {

	        window.fbAsyncInit = function () {
	           // alert('t');
	            // init the FB JS SDK
	            FB.init({
	                appId: '390120407757859', // App ID
	                channelUrl: 'channel.html', // Channel File
	                status: true, // check login status
	                cookie: true, // enable cookies to allow the server to access the session
	                xfbml: true  // parse XFBML

	            });
	         //   alert('initial');
	            FB.getLoginStatus(function (response) {
	                //   alert('ttt');
	                if (response.status === 'connected') {
	                    //   alert('tttc');
	                    // the user is logged in and has authenticated your
	                    // app, and response.authResponse supplies
	                    // the user's ID, a valid access token, a signed
	                    // request, and the time the access token 
	                    // and signed request each expire
	                    var uid = response.authResponse.userID;
	                    var accessToken = response.authResponse.accessToken;
	                    //    alert('before01');
	                    testAPI(response);
	                } else if (response.status === 'not_authorized') {
	                    //    alert('ttt');
	                    // the user is logged in to Facebook, 
	                    // but has not authenticated your app
	                    FB.login(function (response) {
	                        //      alert('before11');
	                        testAPI(response);
	                    }, { scope: 'email' });


	                } else {
	                    // the user isn't logged in to Facebook.
	                    FB.login(function (response) {
	                        //   alert('before12');
	                        testAPI(response);
	                    }, { scope: 'email' });
	                }
	            });
	            //   alert('halfdone');
	            // Additional initialization code such as adding Event Listeners goes here
	        };
	        //   alert('end');
	        // Load the SDK asynchronously
	        (function (d, s, id) {
	            var js, fjs = d.getElementsByTagName(s)[0];
	            if (d.getElementById(id)) { return; }
	            js = d.createElement(s); js.id = id;
	            js.src = "//connect.facebook.net/en_US/all.js";
	            fjs.parentNode.insertBefore(js, fjs);
	        }(document, 'script', 'facebook-jssdk'));
	        // alert('done');
	    }


	    function testlogin() {
	        //  alert('login');
	        FB.getLoginStatus(function (response) {
	            if (response.status === 'connected') {
	                // the user is logged in and has authenticated your
	                // app, and response.authResponse supplies
	                // the user's ID, a valid access token, a signed
	                // request, and the time the access token 
	                // and signed request each expire
	                var uid = response.authResponse.userID;
	                var accessToken = response.authResponse.accessToken;
	                //alert('login01');
	                testAPI(response);
	            } else if (response.status === 'not_authorized') {
	                // the user is logged in to Facebook, 
	                // but has not authenticated your app
	                FB.login(function (response) {
	                //    alert('login11');
	                    testAPI(response);
	                }, { scope: 'email' });


	            } else {
	                // the user isn't logged in to Facebook.
	                FB.login(function (response) {
	                //    alert('login12');
	                    testAPI(response);
	                }, { scope: 'email' });
	            }
	        });
	        // Additional initialization code such as adding Event Listeners goes here


	        // Load the SDK asynchronously
	        (function (d, s, id) {
	            var js, fjs = d.getElementsByTagName(s)[0];
	            if (d.getElementById(id)) { return; }
	            js = d.createElement(s); js.id = id;
	            js.src = "//connect.facebook.net/en_US/all.js";
	            fjs.parentNode.insertBefore(js, fjs);
	        }(document, 'script', 'facebook-jssdk'));
	    }


	    var name;
	    var email;
	    function testAPI(response) {
	        FB.api('/me', function (response) {
	            firstname = response.first_name;
	            lastname = response.last_name;
	            id = response.id;
	            if (firstname != null && firstname != "" && firstname != "undefined") {
	                $("#fbfirstname").val(firstname);
	                $("#fblastname").val(lastname);
	                $("#fbpassword").val(id);
	            }
	            else {
	            }
	            //alert('out');
	        });
	        FB.api('/me?fields=email', function (response) {

	            email = response.email;
	            if (email != null && email != "" && email != "undefined") {
	              //  alert(email);
	                $("#fbemail").val(email);
	                return false;
	            }
	            else {

	                return false;
	            }
	        });

	    }


	</script>
        <script src="js/validateform.js" type="text/javascript"></script>

</head>
<body style="zoom: 1;" cz-shortcut-listen="true">
   
    <form id="form1" runat="server">
        <div class="container">
            <div class="row">
                <!-- Signin div -->
                <div class="span5 form-signin">
                    <h2 class="form-signin-heading">Please sign in</h2>
                    <asp:panel ID="invalidLoginPanel" Visible="false" runat="server" CssClass="alert alert-error" ><%= getLoginErrors() %></asp:panel>
                    <asp:TextBox ID="emailText" runat="server" CssClass="input-block-level" placeholder="Email address"></asp:TextBox>
                    <asp:TextBox ID="passwordText" TextMode="Password" runat="server" CssClass="input-block-level" placeholder="Password"></asp:TextBox>
<%--                    <label class="checkbox">
                        <input type="checkbox" value="remember-me" />
                        Remember me <small class="pull-right"><a href="#">Forgot your password?</a></small>
                    </label>--%>
                    <asp:Button ID="loginButton" CssClass="btn btn-large btn-info" runat="server" Text="Login" OnClick="validateLogin" />
 <!--                    <Button class="btn btn-large btn-primary pull-right" Text="Facebook" OnClientClick="login();" />  -->
                  <!--  <asp:Button ID="Button1" CssClass="btn btn-large btn-primary pull-right" runat="server" Text="Facebook" OnClientClick="return play();" OnClick="FacebookUser" />  
                        
                    <button type="button" id="Button4" runat="server" class="btn btn-large btn-primary pull-right" onclick="return test();" onserverclick="FacebookUser">Facebook Login </button>  -->
                     <a class="btn btn-large btn-primary pull-right" onclick="facebookfunc()">Facebook Login</a>
                </div>

                <div class="span1" />

                <!-- Register div -->
                <div class="span5 form-signin form-register">
                    <h2 class="form-signin-heading">Register... It's Free!</h2>
                    <asp:Panel ID="failedRegistration" runat="server" Visible="false" CssClass="alert alert-error"><%= getRegistrationErrors() %></asp:Panel>
                    
                    <asp:TextBox ID="emailTextRegister" CssClass="input-block-level" placeholder="Email address" runat="server" />
                    <asp:TextBox ID="firstNameTextRegister" CssClass="input-medium" placeholder="First" runat="server" />
                    <asp:TextBox ID="lastNameTextRegister" CssClass="input-large pull-right" placeholder="Last" runat="server" />
                    <hr />
                    <asp:TextBox ID="passwordTextRegister" runat="server" TextMode="Password" CssClass="input-block-level" placeholder="Password" />
                    <asp:TextBox ID="confirmPasswordRegister" runat="server" TextMode="Password" CssClass="input-block-level" placeholder="Confirm Password" />
                    <hr />
                    <asp:Button ID="Button2" CssClass="btn btn-large btn-info" runat="server" Text="Register" OnClick="registerUser" />
                </div>

            </div>
        </div>
  

    
                <div id="fbloginform">
                    
                                    <!-- <button onclick="test();return false;">Test</button>-->

               </div>

       <!-- Choose board for favorite popup -->
        <div id="modal-facebook" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>Facebook Login </h3>
            </div>
            <div class="modal-footer">
                <asp:Button ID="fbbutton1" CssClass="btn btn-primary" runat="server" Text="Board" OnClick="FacebookUserBoard" />
                <asp:Button ID="fbbutton" CssClass="btn btn-primary" runat="server" Text="Dashboard" OnClick="FacebookUser" />
            </div>
            
            <script>
                function facebookfunc() {
                    test();
                         //               $("#fbemail").val('facebook@uptacks.com');
                       //                 $("#fbpassword").val('121212121');
                     //                   $("#fbfirstname").val('Chirag');
                   //                     $("#fblastname").val('Arora');
                    $("#modal-facebook").modal('show');
                }


            </script>           
            <input type="hidden" id="fbemail" name="fbemail" runat="server" />
                    <input type="hidden" id="fbpassword" name="fbpassword" runat="server" />
                    <input type="hidden" id="fbfirstname" name="fbfirstname" runat="server" />
                    <input type="hidden" id="fblastname" name="fblastname" runat="server"/>


        </div>
        </form>
</body>
</html>
