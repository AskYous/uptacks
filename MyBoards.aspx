﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MyBoards.aspx.cs" Inherits="MyBoards" %>

<%@ Register Src="~/Navbar.ascx" TagPrefix="uc1" TagName="Navbar" %>


<!DOCTYPE html>

<head id="Head1" runat="server">
    <title>My Dashboard</title>
    <link href="css/menu-style.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap-responsive.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/script.js"></script>
    <style>
        #pageTitle{
            padding-top:40px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <!-- Navbar -->
        <uc1:Navbar runat="server" ID="Navbar" />
        <script>
            $('#navbar li:nth-child(2)').addClass('active');
        </script>

        <div class="container">
            <div id="errorDeletingBoard" class="alert alert-danger fade in" runat="server">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Error: </strong>You can't delete that board.
            </div>

            <h2 id="pageTitle">My Boards</h2>
            <hr />
            <div class="tabbable tabs-left table-striped ">
                <div class="row-fluid">
                    <ul class="boards">
                        <li class="span3">
                            <a href="#modal-createNewBoard" role="button" data-toggle="modal">
                                <div class="thumbnail">
                                    <img id="create-new" src="img/plus.png" />
                                    <div class="caption">
                                        <h3>Create New Board</h3>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <!-- For each user's board -->
                        <% List<Board> boards = Users.getUserByID(Session["userID"].ToString()).getBoards();
                           foreach (Board b in boards)
                           { %>
                        <!-- Boards -->
                        <li class="span3">
                            <div class="thumbnail" id="board-<% Response.Write(b.getID()); %>">
                                <div class="board-title"><% Response.Write(b.getTitle()); %></div>
                               <!-- <img alt="" src="<% Response.Write(Server.MapPath("~/img/")+Session["userID"].ToString()+"/"+b.getTitle()+"/"+"Jellyfish.jpg"); %>" />
                                 <asp:Image id="Image1" runat="server" 
                                     /> 
                                <img id="Image-<% Response.Write(b.getID()); %>" src="<%= "/uptacks"+b.getImgURL() %>"
                                     />-->
                                   <img id="Img1-<% Response.Write(b.getID()); %>" src="<%= b.getImgURL() %>"
                                     />
                                <!--<div class="caption"> src="<%Response.Write(Server.MapPath("~/img/") + Session["userID"].ToString() + "/" + b.getTitle() + "/" + "Jellyfish.jpg"); %>" </div>
                                -->
                                <div class="caption">
                                    <p style="height: 40px;"><% Response.Write(b.getDescription()); %></p>
                                    <br />
                                    <hr />
                                    <!-- <textarea> Comment... </textarea> -->
                                    <a href="board.aspx?id=<%= b.getID() %>" id="viewBoardButton" class="btn btn-primary pull-right">View Board</a>
                                    <!-- Edit Button -->
                                    <div class="btn-group dropup">
                                        <a id="editBoardButton" class="btn dropdown-toggle" data-toggle="dropdown" href="#">Edit 
                                            <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a class="edit-board-link" href="#"
                                                    data-board-id="<%= b.getID() %>"
                                                    data-board-title="<%= b.getTitle() %>"
                                                    data-board-description="<%= b.getDescription() %>"
                                                    data-board-is-private="<%= b.getPrivacy() %>"
                                                    data-board-img-url="<%= b.getImgURL() %>">
                                                    <strong>Update</strong> Board</a>
                                            </li>
                                            <li>
                                                <a href="#modal-deleteBoard" role="button" data-toggle="modal"
                                                    onclick="
                                                        $('#deleteBoardId').val('<% Response.Write(b.getID()); %>');
                                                        $('#modalBody-boardTitle').html('<% Response.Write(b.getTitle()); %>');
                                                        $('#modal-deleteBoard').modal('show');
                                                    "><strong>Delete</strong> Board
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <% } %>
                        <!-- /Boards -->
                    </ul>
                    <!-- variables for server -->
                    <asp:HiddenField ID="deleteBoardId" runat="server" />
                    <asp:HiddenField ID="editBoardId" runat="server" />
                    <asp:HiddenField ID="viewBoardId" runat="server" />
                </div>
            </div>
        </div>

        <!-- Pop ups -->

        <!-- Create board popup -->
        <div id="modal-createNewBoard" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Create A Board</h3>
            </div>
            <div class="modal-body">
                <asp:Label ID="boardError" runat="server" Visible="false" CssClass="alert alert-error" Text="">.</asp:Label>
                <asp:TextBox runat="server" ID="boardTitle" placeholder="Board Name. 12 characters long, letters and numbers only" Width="90%"/><br />
                <asp:TextBox runat="server" ID="boardDescription" TextMode="MultiLine" placeholder="Description" Width="60%"/><br />
                <div class="btn-group" data-toggle="buttons-radio">
                    <a id="boardIsPublic" class="btn btn-primary active" onclick="$('#isPrivate').val('false')">Public</a>
                    <a id="boardIsPrivate" class="btn btn-primary" onclick="$('#isPrivate').val('true')">Private</a>
                </div>
                <asp:HiddenField runat="server" ID="isPrivate" Value="false" />
                <br />
                <br />
                <asp:FileUpload ID="boardImg" runat="server" placeholder="Board Image URL. The image that will appear on the board." Width="60%"/><br />
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <asp:Button ID="Button1" runat="server" OnClick="createNewBoard" CssClass="btn btn-primary" Text="Create Board" />
            </div>
        </div>

        <!-- Delete board popup -->
        <div id="modal-deleteBoard" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>Delete Board?</h3>
            </div>
            <div class="modal-body">
                <p>
                    Warning, you are deleting the board <strong><span id="modalBody-boardTitle"></span></strong>.
                </p>
            </div>
            <div class="modal-footer">
                <asp:Button ID="Button2" CssClass="btn btn-danger" runat="server" Text="Delete Board" OnClick="deleteBoard" />
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
            </div>
        </div>

        <!-- Edit board popup -->
        <div id="modal-editBoard" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>Edit Board </h3>
            </div>
            <div class="modal-body">
                <label for="editBoardTitle">Board Title </label>
                <input type="text" id="editBoardTitle" runat="server" /><br />
                <label for="editBoardDescription">Board Description </label>
                <textarea id="editBoardDescription" runat="server"></textarea><br />
                <div class="btn-group" data-toggle="buttons-radio">
                    <a id="editBoardIsPublic" class="btn btn-primary" onclick="$('#isPrivate').val('false')">Public</a>
                    <a id="editBoardIsPrivate" class="btn btn-primary" onclick="$('#isPrivate').val('true')">Private</a>
                </div>
                <br />
                <label for="editBoardImgURL">Image Url</label>
                <input id="editBoardImgURL" type="text" runat="server" />
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <asp:Button ID="Button3" CssClass="btn btn-primary" runat="server" Text="Save" OnClick="editBoard" />
            </div>
        </div>

        <!-- Javascript -->
        <%if (boardError.Visible == true)
          { %>
        <script>
            $('#modal-createNewBoard').modal('show');
            console.log("showModal");
        </script>
        <%} %>
    </form>
    <script>
        //User wants to edit a board:
        $(".edit-board-link").on("click", function () {
            boardTitle = $(this).attr('data-board-title');
            boardDescription = $(this).attr('data-board-description');
            boardIsPrivate = $(this).attr('data-board-is-private');
            boardId = $(this).attr('data-board-id');
            boardImgURL = $(this).attr('data-board-img-url');

            $("#editBoardId").val(boardId);
            $('#editBoardTitle').val(boardTitle);
            $('#editBoardImgURL').val(boardImgURL);
            $('#editBoardDescription').html(boardDescription);
            if (boardIsPrivate === "False") {
                $("#editBoardIsPublic").addClass('active');
            } else {
                $("#editBoardIsPrivate").addClass('active');
            }

            $("#modal-editBoard").modal('show');
        });
    </script>
</body>
</html>
