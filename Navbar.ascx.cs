﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Navbar : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        passwordError.Visible = false;
    }

    protected void changePassword(object sender, EventArgs e)
    {
        string password = currentPassword.Text;
        string passwordNew = newPassword.Text;
        string passwordNewConfirm = newPasswordConfirm.Text;

        User owner = Users.getUserByID(Session["userID"].ToString());

        if (passwordNew.Equals(passwordNewConfirm)) //new password has been typed twice and is exact
        {
            owner.changePassword(password, passwordNew);
        }
        else
        {
            passwordError.Visible = true;
        }
    }

    protected void logout(object sender, EventArgs e)
    {
        Session.Clear();
        Response.Redirect("http://uptacks.net");
    }

    protected void search(object sender, EventArgs e)
    {
        Response.Redirect("search.aspx?search=" + searchbox.Text);
    }
}