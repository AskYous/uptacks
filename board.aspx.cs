﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class board : System.Web.UI.Page
{
    public string imgURL;
    public string directory;
    public string title;
    public string filename;
    public string boardimgURL;
    protected User currentUser;

    protected void Page_Load(object sender, EventArgs e)
    {
        tackError.Visible = false;
        errorDeletingTack.Visible = false;
        if (Session["userID"] == null)
        {
            Response.Redirect("login.aspx");
        }
        else if (Session["userID"] != null)
        {
            currentUser = Users.getUserByID(Session["userID"].ToString());

        }
    }

    public void createNewTack(object sender, EventArgs e)
    {
        string title = tackTitle.Text;
        string description = tackDescription.Text;
        string websiteURL = webURL.Text;
        string imgURL = tackImg.Text;
        string imgFile = "";
        string boardID = Request.QueryString["id"];
        string userID = Session["userID"].ToString();
        string fakepath;
        string errors = "";

        User currentUser = Users.getUserByID(userID);

        boardimgURL = Boards.getBoardById(boardID).getImgURL();

        //cuz mohammed needs to complete the regex validation! :)
        Regex titleRegex = new Regex(@"^[A-Za-z0-9 ]{1,}$");
        Match titleValidation = titleRegex.Match(title);
        if (!titleValidation.Success)
        {
            errors = errors + "* Title should consist of letters and numbers only<br />";

        }
        if (title.Length > 12)
        {
            errors = errors + "* Title should be 12 charecters long<br />";
        }
        if (description == "")
        {
            errors = errors + "* Description field is required<br />";

        }
        if (title == "" || description == "" || (imgURL == "" && tackboardImg.FileName == "")|| websiteURL == "")
        {
            errors = errors + "* Please fill out everything. All fields are required.";
        }

        if (errors == "")
        {
            if (!websiteURL.Contains("http"))
            {
                websiteURL = "http://" + websiteURL;
            }
            if (tackboardImg.PostedFile.ContentType.Contains("image"))
            {
                filename = Path.GetFileName(tackboardImg.FileName);

                directory = boardimgURL;
                string[] substrings = Regex.Split(directory, "/");
                //Image1.ImageUrl = substrings[1] + substrings[2] + substrings[3] + substrings[4];
                directory = Server.MapPath("~/") + substrings[1] + "/" + substrings[2] + "/" + substrings[3];
                string imgpath = "/img/" + substrings[2] + "/" + substrings[3];
                string targetPath = directory + "/" + filename; //with complete path
                fakepath = directory + "/" + substrings[3];

                bool isExists = System.IO.File.Exists(targetPath);
                string bp = "";
                if (isExists)
                {
                    System.IO.Directory.CreateDirectory(fakepath);

                    tackboardImg.SaveAs(fakepath + "/" + filename);
                    imgURL = imgpath + "/" + substrings[3] + "/" + filename;
                    //Image1.ImageUrl = fakepath + "/" + filename;
                    //name.ImageUrl="";
                    //                    boardImg.SaveAs(targetPath+"/" + filename);
                    //("~/") + imgURL
                }
                else
                {
                    bp = directory;
                    tackboardImg.SaveAs(bp + "/" + filename);
                    imgURL = imgpath + "/" + filename;
                    //Image1.ImageUrl = bp + "/" + filename;

                }

            }
            else
            {
                imgURL = tackImg.Text;
            }
        }
        if (errors != "")
        {
            tackError.Text = errors;
            tackError.Visible = true;
            return;
        }
        else if (Tacks.createNewTack(userID, title, description, imgURL, boardID, websiteURL))
        {
            Response.Redirect("board.aspx?id=" + boardID);
        }
        else
        {
            tackError.Text = "You already have a tack with that name!";
            tackError.Visible = true;
        }
        
    }

    protected void deleteTack(object sender, EventArgs e)
    {
        string tackIdToDelete = deleteTackId.Value;
        User owner = Users.getUserByID(Session["userID"].ToString());
        if (Tacks.getTackById(tackIdToDelete) != null)
        {
            Tacks.deleteTackById(tackIdToDelete);
        }
        else
        {
            errorDeletingTack.Visible = true;
        }
    }
    protected void followBoard(object sender, EventArgs e)
    {
        string boardIDTofollow = Request.QueryString["id"];
        User owner = Users.getUserByID(Session["userID"].ToString());
        Board board = Boards.getBoardById(boardIDTofollow);
        if (!board.isFollowed(owner))
        {
            board.follow(owner);
        }
    }

    protected void editTack(object sender, EventArgs e)
    {
        Tack tackToEdit = Tacks.getTackById(editTackId.Value);

        string newTackTitle = editTackTitle.Value;
        string newTackDescription = editTackDescription.Value;
        string newTackImgURL = editTackImgURL.Value;

        tackToEdit.setTitle(newTackTitle);
        tackToEdit.setDescription(newTackDescription);
        tackToEdit.setImgURL(newTackImgURL);
        tackToEdit.commitChanges();
    }
    protected void submit(object sender, EventArgs e)
    {

        Comment.insertComment(Session["userID"].ToString(), commentTackId.Value, Request.Form["commentText"], Session["fullName"].ToString());
    }

    protected void retack_Click(object sender, EventArgs e)
    {
        string id = retackID.Value;
        Tack tackToRetack = Tacks.getTackById(id);
        Tacks.createNewTack(Session["userID"].ToString(), tackToRetack.getTitle(), tackToRetack.getDescription(), tackToRetack.getImgURL(), toBoardID.Value, tackToRetack.getWebsiteURL());
    }

    /*    protected void favorite_Click(object sender, EventArgs e)
        {
            string favid = favoriteid.Value;
            Tack favorite = Tacks.getTackById(favid);
            string userID = Session["userID"].ToString();
            User currentUser = Users.getUserByID(userID);
            //        User current = Users.getUserByID(Session["userID"].ToString());
            favorite.favorite(currentUser, favorite);
        }
    */
    protected void favorite_Click(object sender, EventArgs e)
    {
        string favid = favoriteid.Value;
        Tack favorite = Tacks.getTackById(favid);
        //        User current = Users.getUserByID(Session["userID"].ToString());
        favorite.favorite(currentUser, favorite);
    }
}