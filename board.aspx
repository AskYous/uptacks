﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="board.aspx.cs" Inherits="board" %>

<%@ Register Src="~/Navbar.ascx" TagPrefix="uc1" TagName="Navbar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><%= Boards.getBoardById(Request.QueryString["id"]).getTitle() %></title>
    <%
        if (Boards.getBoardById(Request.QueryString["id"].ToString()).getPrivacy()
            && Boards.getBoardById(Request.QueryString["id"].ToString()).getOwner() != Session["userID"].ToString())
        {
            Response.Redirect("dashboard.aspx");
        }
    %>
    <link href="css/menu-style.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap-responsive.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/script.js"></script>
</head>
    <style>
        .cover-photo{
            padding-top:40px;
        }
        #blackheart{
            height: 33px;
            width: 39px;
            padding-top: 40px;
        }

        @-webkit-keyframes beat {
            0% {
                -webkit-transform: scale(1) rotate(225deg);
                -webkit-box-shadow: 0 0 40px rgba(213,9,60,1);
            }

            50% {
                -webkit-transform: scale(1.1) rotate(225deg);
                -webkit-box-shadow: 0 0 70px rgba(213,9,60,0.6);
            }

            100% {
                -webkit-transform: scale(1) rotate(225deg);
                -webkit-box-shadow: 0 0 40px rgba(213,9,60,1);
            }
        }

        @-moz-keyframes beat {
            0% {
                -moz-transform: scale(1) rotate(225deg);
                -moz-box-shadow: 0 0 40px rgba(213,9,60,1);
            }

            50% {
                -moz-transform: scale(1.1) rotate(225deg);
                -moz-box-shadow: 0 0 70px rgba(213,9,60,0.6);
            }

            100% {
                -moz-transform: scale(1) rotate(225deg);
                -moz-box-shadow: 0 0 40px rgba(213,9,60,1);
            }
        }

        @keyframes beat {
            0% {
                transform: scale(1) rotate(225deg);
                box-shadow: 0 0 40px #d5093c;
            }

            50% {
                transform: scale(1.1) rotate(225deg);
                box-shadow: 0 0 70px #d5093c;
            }

            100% {
                transform: scale(1) rotate(225deg);
                box-shadow: 0 0 40px #d5093c;
            }
        }

        #chest {
            position: relative;
            width: 50px;
            height: 45px;
            margin: 0 auto;
            margin-top: -35px;
            cursor: pointer;
        }

        .heart {
            position: absolute;
            z-index: 2;
            background: -moz-linear-gradient(-180deg, #F50A45 0%, #d5093c 40%);
            background: -webkit-gradient(linear, right 50%, left 50%, color-stop(0%,#F50A45), color-stop(40%,#d5093c));
            background: -webkit-linear-gradient(-180deg, #F50A45 0%,#d5093c 40%);
            background: linear-gradient(-180deg, #F50A45 0%,#d5093c 40%);
            -webkit-animation: beat 0.7s ease 0s infinite normal;
            -moz-animation: beat 0.7s ease 0s infinite normal;
            animation: beat 0.7s ease 0s infinite normal;
        }

            .heart.center {
                background: -moz-linear-gradient(-45deg, #B80734 0%, #d5093c 40%);
                background: -webkit-gradient(linear, left top, right bottom, color-stop(0%,#B80734), color-stop(40%,#d5093c));
                background: -webkit-linear-gradient(-45deg, #B80734 0%,#d5093c 40%);
                background: linear-gradient(-45deg, #B80734 0%,#d5093c 40%);
            }

            .heart.top {
                z-index: 3;
            }

        .side {
            top: 38px;
            width: 22px;
            height: 22px;
            -moz-border-radius: 220px;
            -webkit-border-radius: 220px;
            border-radius: 220px;
        }

        .center {
            width: 21px;
            height: 21px;
            bottom: -24px;
            left: 14px;
            font-size: 0;
            text-indent: -9999px;
        }

        .left {
            left: 22px;
        }

        .right {
            right: 23px;
        }
    </style>

<body>
    <form id="form1" runat="server">
        <uc1:Navbar runat="server" ID="Navbar" />
        <div class="container">
            <div id="errorDeletingTack" class="alert alert-danger fade in" runat="server">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Error: </strong>You can't delete that tack.
            </div>
            <div class="cover-photo">
                <img src="<%=Boards.getBoardById(Request.QueryString["id"]).getImgURL()  %>" />
            </div>
            <br />
            <br />
            <h2>Viewing Board: <%= Boards.getBoardById(Request.QueryString["id"]).getTitle() %></h2>
            <hr />
            <div class="tabbable tabs-left table-striped ">
                <div class="row-fluid">
                    <ul class="tacks">
                        <% Board b = Boards.getBoardById(Request.QueryString["id"]);
                           if (b.getOwner() == Session["userID"].ToString())
                           { %>
                        <li class="span3">
                            <a href="#modal-createNewTack" role="button" data-toggle="modal">
                                <div class="thumbnail">
                                    <img id="create-new" src="img/plus.png" />
                                    <div class="caption">
                                        <h3>Create New Tack</h3>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <% }
                           else if (!b.isFollowed(Users.getUserByID(Session["userID"].ToString())))
                           {
                           
                        %>
                        <li class="span3">
                            <a href="#modal-followBoard" role="button" data-toggle="modal">
                                <div class="thumbnail">
                                    <img id="create-new" src="http://goalsettingsecondstep.com/images/come_here_lg_clr.gif" />
<%--                                    <img id="create-new" src="http://facebookinsiders.com/wp-content/uploads/2012/09/wpid-67c52d394d406bb992fa2b48a18edf82.jpg" />--%>
                                    <div class="caption">
                                        <h3>Follow this board</h3>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <% } %>

                        <!-- For each user's tack -->
                        <%   
                            string boardID = Request.QueryString["id"];
                            List<Tack> tacks = Boards.getBoardById(boardID).getTacks();

                            foreach (Tack t in tacks)
                            {
                        %>
                        <!-- Tacks -->
                        <li class="span3">
                            <div class="thumbnail" id="tack-<% Response.Write(t.getID()); %>">
                                <div class="tack-title"><% Response.Write(t.getTitle()); %></div>
                                <% if (!t.getWebsiteURL().Contains("youtube"))
                                   { %>
                                <a href="<%= t.getWebsiteURL() %>">
                                    <img alt="" src="<%= t.getImgURL() %>" /></a>
                                <%}
                                   else if (t.getWebsiteURL().Contains("youtube"))
                                   { %>
                                <a href="#modal-youtubeVideo" role="button" data-toggle="modal"
                                    data-tack-id="<%= t.getID() %>"
                                    data-tack-title="<%= t.getTitle() %>"
                                    data-tack-description="<%= t.getDescription() %>"
                                    data-tack-youtube-id="<%= t.getYoutubeID() %>">
                                    <img alt="" src="<%= t.getImgURL() %>" /></a>
                                <% } %>
                                <div class="caption">
                                    <p style="height: 40px;"><% Response.Write(t.getDescription()); %></p>
                                    <br />
                                    <hr />


                                    <!--comments-->
                                    <%List<String> showComments = Comment.getComment(t.getID());%>
                                    <div style="width: 247px; height: 150px; overflow: auto; margin: 2px; color: #000000; scrollbar-base-color: #DEBB07;">
                                        <% foreach (String c in showComments)
                                           {                        
                                        %>
                                        <p style="width: 215px; color:white"><% Response.Write(c); %></p>

                                        <%} %>
                                    </div>

                                    <textarea id="commentText" name="commentText" onfocus="checkComment(<%= t.getID()%>)"></textarea>
                                    <div id='chest' onclick='favorite("<%= t.getID() %>","<%= System.Web.HttpUtility.HtmlEncode(t.getTitle()) %>")'>
                                        <% if (t.isFavorited(currentUser)){ %>
                                        <div class="heart left side top"></div>
                                        <div class="heart center">&hearts;</div>
                                        <div class="heart right side"></div>
                                        <% } else { %>
                                        <img id="blackheart" alt="" src="img/theblackheart.png" />
                                        <% } %>
                                    </div>
                                    <asp:Button ID="Button4" CssClass="btn btn-primary pull-right commentButton" OnClick="submit"
                                        Text="Comment" runat="server" />

                                    <div class="btn-group dropup">
                                        <!-- Edit Button -->
                                        <% if (b.getOwner() == Session["userID"].ToString())
                                           { %>
                                        <a id="editTackButton" class="btn dropdown-toggle" data-toggle="dropdown" href="#">Edit 
                                            <span class="caret"></span>
                                        </a>
                                        <!-- favourite -->

                                        <ul class="dropdown-menu">
                                            <li>
                                                <a class="edit-tack-link" href="#"
                                                    data-tack-id="<%= t.getID() %>"
                                                    data-tack-title="<%= t.getTitle() %>"
                                                    data-tack-description="<%= t.getDescription() %>"
                                                    data-tack-img-url="<%= t.getImgURL() %>">
                                                    <strong>Update</strong> Tack</a>
                                            </li>
                                            <li>
                                                <a href="#modal-deleteTack" role="button" data-toggle="modal"
                                                    onclick="
                                                        $('#deleteTackId').val('<% Response.Write(t.getID()); %>');
                                                        $('#modalBody-tackTitle').html('<% Response.Write(t.getTitle()); %>');
                                                        $('#modal-deleteTack').modal('show');
                                                    "><strong>Delete</strong> Tack
                                                </a>
                                            </li>
                                        </ul>
                                        <% }
                                           else
                                           { %>
                                        <!-- Retack Button -->
                                        <a class="btn btn-success" onclick="reTack('<%= t.getID() %>','<%= t.getTitle() %>')">Re-tack</a>


                                        <% } %>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <% } %>
                        <!-- /Tacks -->
                    </ul>
                    <!-- variables for server -->
                    <asp:HiddenField ID="deleteTackId" runat="server" />
                    <asp:HiddenField ID="editTackId" runat="server" />
                    <asp:HiddenField ID="commentTackId" runat="server" />
                    <asp:HiddenField ID="commentUserId" runat="server" />
                    <asp:HiddenField ID="commentTextValue" runat="server" />
                </div>
            </div>
        </div>

        <!-- Pop ups -->

        <!-- Youtube Video popup -->
        <div id="modal-youtubeVideo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width:auto">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="VTT">Video</h3>
                <hr />
                <br />
                <iframe id="youtubeplayer" width="560" height="315" src="//www.youtube.com/embed/98BIu9dpwHU" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="modal-body">
                <asp:Label ID="Label2" runat="server" Visible="false" CssClass="alert alert-error" Text="">.</asp:Label>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div>

        <!-- Create tack popup -->
        <div id="modal-createNewTack" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Create A Tack</h3>
            </div>
            <div class="modal-body">
                <asp:Label ID="tackError" runat="server" Visible="false" CssClass="alert alert-error" Text="">.</asp:Label>
                <asp:TextBox runat="server" ID="tackTitle" placeholder="Tack Name. 12 characters long, letters and numbers only" Width="95%"/><br />
                <asp:TextBox runat="server" ID="tackDescription" TextMode="MultiLine" placeholder="Description" Width="60%"/><br />
                <asp:TextBox runat="server" ID="webURL" placeholder="Website URL. The Website the tack will link to." Width="60%"/><br />
                <asp:TextBox runat="server" ID="tackImg" placeholder="Image URL. The image that will appear on the tack." Width="60%"/><br />
                <asp:FileUpload ID="tackboardImg" runat="server" placeholder="Tack Image" /><br />


            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <asp:Button ID="Button1" runat="server" OnClick="createNewTack" CssClass="btn btn-primary" Text="Create Tack" />
            </div>
        </div>

        <!-- Confirm follow -->
        <div id="modal-followBoard" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Like this board?</h3>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">No, I don't like it</button>
                <asp:Button runat="server" OnClick="followBoard" CssClass="btn btn-primary" Text="Follow" />
            </div>
        </div>

        <!-- Delete tack popup -->
        <div id="modal-deleteTack" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>Delete Tack?</h3>
            </div>
            <div class="modal-body">
                <p>
                    Warning, you are deleting the tack <strong><span id="modalBody-tackTitle"></span></strong>.
                </p>
            </div>
            <div class="modal-footer">
                <asp:Button ID="Button2" CssClass="btn btn-danger" runat="server" Text="Delete Tack" OnClick="deleteTack" />
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
            </div>
        </div>

        <!-- Edit tack popup -->
        <div id="modal-editTack" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>Edit Tack </h3>
            </div>
            <div class="modal-body">
                <label for="editTackTitle">Tack Title </label>
                <input type="text" id="editTackTitle" runat="server" /><br />
                <label for="editTackDescription">Tack Description </label>
                <textarea id="editTackDescription" runat="server"></textarea><br />
                <label for="editTackImgURL">Image Url</label>
                <input id="editTackImgURL" type="text" runat="server" />
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <asp:Button ID="Button3" CssClass="btn btn-primary" runat="server" Text="Save" OnClick="editTack" />
            </div>
        </div>

        <!-- Choose board for reTack popup -->
        <div id="modal-editBoard" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>Choose board for Tack: <strong><span id="chooseBoard"></span></strong></h3>
            </div>
            <div class="modal-body">
                <label for="selectBoard">Select Board: </label>
                <select id="boardSelect">
                    <%
                        foreach (Board bo in Users.getUserByID(Session["userID"].ToString()).getBoards())
                        {
                    %>

                    <option value="<%= bo.getID() %>"><%= bo.getTitle() %></option>

                    <%
                        }
                    %>
                </select>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <asp:Button ID="retackBtn" CssClass="btn btn-primary" OnClientClick="$('#toBoardID').val($('#boardSelect').val())" runat="server" Text="Save" OnClick="retack_Click" />
            </div>

            <script>
                function reTack(id, title) {
                    $("#retackID").val(id);
                    $("#chooseBoard").html(title);
                    $("#modal-editBoard").modal('show');
                }


            </script>

            <input type="hidden" id="retackID" runat="server" />
            <input type="hidden" id="toBoardID" runat="server" />

        </div>

        <!-- Choose board for favorite popup -->
        <div id="modal-favorite" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>You sure you want to make it Favorite? </h3>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                <asp:Button ID="favbutton" CssClass="btn btn-primary" runat="server" Text="Yes" OnClick="favorite_Click" />
            </div>

            <script>
                function favorite(id, title) {
                    $("#favoriteid").val(id);
                    $("#favoritetitle").html(title);
                    $("#modal-favorite").modal('show');
                }



                function checkComment(dataTackId) {


                    commentLabel = $('.commentButton').attr('dataTackId');


                    $('#commentTackId').val(dataTackId);



                }

            </script>

            <input type="hidden" id="favoriteid" runat="server" />
            <input type="hidden" id="favoritetitle" runat="server" />

        </div>


        <!-- Javascript -->
        <%if (tackError.Visible == true)
          { %>
        <script>
            $('#modal-createNewTack').modal('show');
            console.log("showModal");
        </script>
        <%} %>
    </form>
    <script>
        //User wants to edit a tack:
        $(".edit-tack-link").on("click", function () {
            tackTitle = $(this).attr('data-tack-title');
            tackDescription = $(this).attr('data-tack-description');
            tackId = $(this).attr('data-tack-id');
            tackImgURL = $(this).attr('data-tack-img-url');

            $("#editTackId").val(tackId);
            $('#editTackTitle').val(tackTitle);
            $('#editTackImgURL').val(tackImgURL);
            $('#editTackDescription').html(tackDescription);

            $("#modal-editTack").modal('show');
        });

        function checkComment(dataTackId) {
            commentLabel = $('.commentButton').attr('dataTackId');
            $('#commentTackId').val(dataTackId);

        }

    </script>
</body>
</html>
