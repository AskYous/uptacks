﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <title>UPTACKS</title>
      <link href="css/bootstrap.min.css" rel="stylesheet" media="screen"/>
      <link href="css/bootstrap-responsive.min.css" rel="stylesheet" media="screen"/>
        <!-- Place somewhere in the  of your document -->
        <link rel="stylesheet" href="css/flexslider.css" type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
        <script src="js/jquery.flexslider.js"></script>
      <style>
        #myCanvasContainer canvas
        {
            background-image: url('img/tagcloudtextbackground.png');
            margin-top: 50px;
        }
        
        .btn-go
        {
            padding: 19px 24px;
            font-size: 24px;
            position: absolute;
            top: 430px;
        }
        
        /* GLOBAL STYLES
          -------------------------------------------------- */
        /* Padding below the footer and lighter body text */
        
        body
        {
            padding-bottom: 40px;
            color: #5a5a5a; /*background: url('img/TagcloudBackground.png');*/
            background-repeat: repeat-x;
        }
        
        /* CUSTOMIZE THE NAVBAR
          -------------------------------------------------- */
        
        /* Special class on .container surrounding .navbar, used for positioning it into place. */
        .navbar-wrapper
        {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            z-index: 10;
            margin-top: 20px;
            margin-bottom: -90px; /* Negative margin to pull up carousel. 90px is roughly margins and height of navbar. */
        }
        .navbar-wrapper .navbar
        {
        }
        
        /* Remove border and change up box shadow for more contrast */
        .navbar .navbar-inner
        {
            border: 0;
            -webkit-box-shadow: 0 2px 10px rgba(0,0,0,.25);
            -moz-box-shadow: 0 2px 10px rgba(0,0,0,.25);
            box-shadow: 0 2px 10px rgba(0,0,0,.25);
        }
        
        /* Downsize the brand/project name a bit */
        .navbar .brand
        {
            padding: 14px 20px 16px; /* Increase vertical padding to match navbar links */
            font-size: 16px;
            font-weight: bold;
        }
        
        /* Navbar links: increase padding for taller navbar */
        .navbar .nav > li > a
        {
            padding: 15px 20px;
        }
        
        /* Offset the responsive button for proper vertical alignment */
        .navbar .btn-navbar
        {
            margin-top: 10px;
        }
        .navbar-innder
        {
            padding-left: 20px;
        }
        
        
        /* MARKETING CONTENT
          -------------------------------------------------- */

        
        /* Center align the text within the three columns below the carousel */
        .marketing .span4
        {
            text-align: center;
        }
        .marketing h2
        {
            font-weight: normal;
        }
        .marketing .span4 p
        {
            margin-left: 10px;
            margin-right: 10px;
        }
        
        
        /* Featurettes
          ------------------------- */
        .slides
        {
            padding:60px;
            padding-bottom:0px;
            padding-top:30px;
        }
        .flexslider .slides img
        {
            padding:50px;
            width: 40%;
        }
        .featurette-divider
        {
            margin: 80px 0; /* Space out the Bootstrap <hr> more */
        }
        .featurette
        {
             width:95% !important;
             padding-top: 120px; /* Vertically center images part 1: add padding above and below text. */
            overflow: hidden; /* Vertically center images part 2: clear their floats. */
        }
        .featurette-image
        {
            margin-top: -120px; /* Vertically center images part 3: negative margin up the image the same amount of the padding to center it. */
            /*width: 40% !important;*/
            height: 100%;
            border-radius: 7px !important;
        }
        
        /* Give some space on the sides of the floated elements so text doesn't run right into it. */
        .featurette-image.pull-left
        {
            /*margin-right: 40px;*/
        }
        .featurette-image.pull-right
        {
            /*margin-left: 40px;*/
        }
        
        /* Thin out the marketing headings */
        .featurette-heading
        {
            font-size: 50px;
            font-weight: 300;
            line-height: 1;
            letter-spacing: -1px;
        }
        
        
        
        /* RESPONSIVE CSS
          -------------------------------------------------- */
        
        @media (max-width: 979px)
        {
        
            .container.navbar-wrapper
            {
                margin-bottom: 0;
                width: auto;
            }
            .navbar-inner
            {
                border-radius: 0;
                margin: -20px 0;
            }
        
            .featurette
            {
                height: auto;
                padding: 0;
            }
            .featurette-image.pull-left, .featurette-image.pull-right
            {
                display: block;
                float: none;
                max-width: 40%;
                margin: 0 auto 20px;
            }
        }
        
        
        @media (max-width: 767px)
        {
        
            .navbar-inner
            {
                margin: -20px;
                padding-left: 20px;
            }
        
            .marketing .span4 + .span4
            {
                margin-top: 40px;
            }
        
            .featurette-heading
            {
                font-size: 30px;
            }
            .featurette .lead
            {
                font-size: 18px;
                line-height: 1.5;
            }

        
        }
            a.flex-prev
            {
                padding-top:10px;
            }
            a.flex-next
            {
                padding-top:10px;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">

    <body>
        <div style="width: 1300px; margin: 0 auto;">
            <%--<div id="myCanvasContainer" style="margin: auto;">
                <a href="login.aspx" class="btn-go btn btn-primary btn-large">Start Tacking Today ></a>
                <canvas width="1300px" height="500px" id="myCanvas">
          <p>Anything in here will be replaced on browsers that support the canvas element</p>
       </canvas>
                <div id="tags">
                    <ul>
                        <li><a href="">
                            <img src="img/tvs/AAS.png" /></a></li>
                        <li><a href="">
                            <img src="img/tvs/AAS.png" /></a></li>
                        <li><a href="">
                            <img src="img/tvs/YOS.png" /></a></li>
                        <li><a href="">
                            <img src="img/tvs/P3D.png" /></a></li>
                        <li><a href="">
                            <img src="img/tvs/P3D.png" /></a></li>
                        <li><a href="">
                            <img src="img/tvs/YOS.png" /></a></li>
                        <li><a href="">
                            <img src="img/tvs/SOTA.png" /></a></li>
                        <li><a href="">
                            <img src="img/tvs/SOTA.png" /></a></li>
                        <li><a href="">
                            <img src="img/tvs/YOS.png" /></a></li>
                        <li><a href="">
                            <img src="img/tvs/AAS.png" /></a></li>
                    </ul>
                </div>
            </div>--%>
        </div>
        <div class="container">
            <div class="navbar-wrapper">
                <!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
                <div class="container">
                    <div class="navbar navbar navbar-fixed-top">
                        <div class="navbar-inner" style="padding-left: 20px;">
                            <!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
                            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                                <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar">
                                </span>
                            </button>
                            <a class="brand">UpTacks</a>
                            <!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
                            <div class="nav-collapse collapse">
<%--                                <ul class="nav">
                                    <li><a href="myBoards.aspx">My Boards</a></li>
                                    <li><a href="dashboard.aspx">My Stream</a></li>
                                </ul>--%>
                                <ul class="nav pull-right">
                                    <li><a style="padding: 10px 20px" href="#">UpTacks brought to you by
                                        <img src="img/upworks_banner.png" style="height: 30px" /></a> </li>
                                </ul>
                            </div>
                            <!--/.nav-collapse -->
                        </div>
                        <!-- /.navbar-inner -->
                    </div>
                    <!-- /.navbar -->
                </div>
                <!-- /.container -->
            </div>
            <!-- /.navbar-wrapper -->
            <!-- Place somewhere in the of your page -->
            <div class="flexslider">
              <ul class="slides container marketing">
                <li class="featurette">
                        <img class="featurette-image pull-right" src="http://icons.iconarchive.com/icons/icons-land/vista-map-markers/256/Map-Marker-Push-Pin-1-Azure-icon.png">
                        <h2 class="featurette-heading">
                            See something cool in the internet? <span class="muted">Tack it!</span></h2>
                        <p class="lead">
                            Tacks are made to share anything you see on the interenet, ever, to whomever you
                            want, ever. All you need is internet. Then invite people to share, re-Tack, like,
                            and comment on your shared Tacks.
                        </p>
                        <a href="login.aspx" class="btn-go btn btn-primary btn-large">Start Tacking Today&raquo;</a>
                    </li>
                <li class="featurette">
                        <img class="featurette-image pull-right" src="http://icons.iconarchive.com/icons/designbolts/cute-social-media/256/Share-icon.png"">
                        <h2 class="featurette-heading">
                            Sharing and Following. <span class="muted">All Tacks are re-Tackable.</span></h2>
                        <p class="lead">
                            See another good Tack on UpTacks? Share it! All Tacks can be shared on your boards
                            while giving credit to the original poster.</p>
                        <a href="login.aspx" class="btn-go btn btn-primary btn-large">Start Tacking Today&raquo;</a>
                    </li>
                <li class="featurette">
                        <img class="featurette-image pull-right" src="http://icons.iconarchive.com/icons/iconleak/or/256/clipboard-icon.png"/>
                        <h2 class="featurette-heading"  style="">
                            No more 1 giant wall of Tacks. <span class="muted">Welcome to Boards</span></h2>
                        <p class="lead"  style="">
                            Organize, categorize, or group Tacks to have all relative tacks in one place. Share
                            boards to friends, family, or make them private only to you.</p>
                        <a href="login.aspx" class="btn-go btn btn-primary btn-large">Start Tacking Today&raquo;</a>
                    </li>
              </ul>
            </div>
<%--            <div class="container marketing">
                <!-- Three columns of text below the carousel -->
                <div class="row" style="margin-top: 15px;">
                    <div class="span4">
                        <img class="!img-circle" style="width: 140px; height: 140px;" src="http://icons.iconarchive.com/icons/icons-land/vista-map-markers/256/Map-Marker-Push-Pin-1-Azure-icon.png" />
                        <h2>
                            Tacks</h2>
                        <p>
                            Share photos, videos, news, links, events, reviews, as Tacks viewable to the world!</p>
                        <p>
                            <a class="btn" onclick="document.getElementById('feat1').scrollIntoView()">View details
                                &raquo;</a></p>
                    </div>
                    <!-- /.span4 -->
                    <div class="span4">
                        <img class="!img-circle" style="width: 140px; height: 140px;" src="http://icons.iconarchive.com/icons/designbolts/cute-social-media/256/Share-icon.png">
                        <h2>
                            Sharing and Following</h2>
                        <p>
                            Share, follow, retack any Tacks you see on UpTacks. Follow any user's Board and
                            their Tacks shared are set to post to your dashboard.</p>
                        <p>
                            <a class="btn" onclick="document.getElementById('feat2').scrollIntoView()">View details
                                &raquo;</a></p>
                    </div>
                    <!-- /.span4 -->
                    <div class="span4">
                        <img class="!img-circle" style="width: 140px; height: 140px;" src="http://icons.iconarchive.com/icons/iconleak/or/256/clipboard-icon.png">
                        <h2>
                            Boards</h2>
                        <p>
                            Organize, categorize, or group Tacks to have all relative tacks in one place. Share
                            boards to friends, family, or make them private only to you.</p>
                        <p>
                            <a class="btn" onclick="document.getElementById('feat3').scrollIntoView()">View details
                                &raquo;</a></p>
                    </div>
                    <!-- /.span4 -->
                </div>
                <!-- /.row -->
                <!-- START THE FEATURETTES -->
                <hr class="featurette-divider" id="feat1">
                <div class="featurette">
                    <img class="featurette-image pull-right" src="http://icons.iconarchive.com/icons/icons-land/vista-map-markers/256/Map-Marker-Push-Pin-1-Azure-icon.png">
                    <h2 class="featurette-heading">
                        See something cool in the internet? <span class="muted">Tack it!</span></h2>
                    <p class="lead">
                        Tacks are made to share anything you see on the interenet, ever, to whomever you
                        want, ever. All you need is internet. Then invite people to share, re-Tack, like,
                        and comment on your shared Tacks.
                    </p>
                </div>
                <hr class="featurette-divider" id="feat2">
                <div class="featurette">
                    <img class="featurette-image pull-left" src="http://icons.iconarchive.com/icons/designbolts/cute-social-media/256/Share-icon.png">
                    <h2 class="featurette-heading">
                        Sharing and Following. <span class="muted">All Tacks are re-Tackable.</span></h2>
                    <p class="lead">
                        See another good Tack on UpTacks? Share it! All Tacks can be shared on your boards
                        while giving credit to the original poster.</p>
                </div>
                <hr class="featurette-divider" id="feat3">
                <div class="featurette">
                    <img class="featurette-image pull-right" src="http://icons.iconarchive.com/icons/iconleak/or/256/clipboard-icon.png">
                    <h2 class="featurette-heading">
                        No more 1 giant wall of Tacks. <span class="muted">Welcome to Boards</span></h2>
                    <p class="lead">
                        Organize, categorize, or group Tacks to have all relative tacks in one place. Share
                        boards to friends, family, or make them private only to you.</p>
                </div>
                <hr class="featurette-divider">
                <!-- /END THE FEATURETTES -->--%>
                <!-- FOOTER -->
                <footer>
<%--                    <p class="pull-right"><a href="#">Back to top</a></p>--%>
                    <p>&copy; 2013 Upworks, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
                  </footer>
                <script src="js/jquery-1.10.2.min.js"></script>
                <script src="js/bootstrap.min.js"></script>
                <!--[if lt IE 9]><script type="text/javascript" src="excanvas.js"></script><![endif]-->
<%--                <script src="js/jquery.tagcanvas.min.js" type="text/javascript"></script>--%>
                <script type="text/javascript">
                    $.noConflict();
                    //$(document).ready(function () {
                    //    if (!$('#myCanvas').tagcanvas({ wheelZoom: false })) {
                    //        // TagCanvas failed to load
                    //        $('#myCanvasContainer').hide();
                    //    }
                    //    // your other jQuery stuff here...
                    //    window.onload = function () {
                    //        try {
                    //            TagCanvas.Start('myCanvas', 'tags', {
                    //                offsetY: 0,
                    //                radiusY: .8,
                    //                radiusX: 2,
                    //                radiusZ: 2,
                    //                wheelZoom: false,
                    //                lock: "y"
                    //            });
                    //        } catch (e) {
                    //            // something went wrong, hide the canvas container
                    //            document.getElementById('myCanvasContainer').style.display = 'none';
                    //        }
                    //    };
                    //});
                </script>
                <!-- Place in the  after the three links -->
                <script type="text/javascript" charset="utf-8">
                    $(window).load(function () {
                        $('.flexslider').flexslider({
                            prevText: "",
                            nextText: ""
                        });
                    });
                </script>
    </form>
</body>
</html>
