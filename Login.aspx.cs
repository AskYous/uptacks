﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{
    private string RegistrationErrors;
    private string LoginErrors;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        { //do something 
            failedRegistration.Visible = false;
            invalidLoginPanel.Visible = false;
        }
    }
    protected void validateLogin(object sender, EventArgs e)
    {
        string email = emailText.Text;
        string password = passwordText.Text;
        if (Users.validateLogin(email, password))
        {
            //valid login. Send user to dashboard.

            User currentUser = Users.getUserByEmail(email);
            Session["userID"] = currentUser.getID();
            Session["fullName"] = currentUser.getFullName();
            Response.Redirect("dashboard.aspx");
        }
        else
        {
            //error message for user
            LoginErrors = "Email and Password do not match";
            invalidLoginPanel.Visible = true;
        }
    }

    public string getLoginErrors()
    {
        return LoginErrors;
    }

    protected void registerUser(object sender, EventArgs e)
    {
        string email = emailTextRegister.Text;
        string firstName = firstNameTextRegister.Text;
        string lastname = lastNameTextRegister.Text;
        string password = passwordTextRegister.Text;
        string confirmPassword = confirmPasswordRegister.Text;

        string errors = "";

        Regex emailRegex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        Match emailValidation = emailRegex.Match(email);
        if (!emailValidation.Success)
        {
            errors = errors + "* Please provide a valid email address<br />";
        }
        Regex nameRegex = new Regex(@"^[A-Za-z0-9]{1,}$");
        Match firstValidation = nameRegex.Match(firstName);
        if (!firstValidation.Success)
        {
            errors = errors + "* First name should consist of letters only<br />";
        }
        Match lastValidation = nameRegex.Match(lastname);
        if (!lastValidation.Success)
        {
            errors = errors + "* Last name should consist of letters only<br />";
        }
        Regex passwordRegex = new Regex(@"^.*(?=.{6,})(?=.*\d)(?=.*[A-Za-z0-9!@#$%^&*()_]).*$");
        Match passwordValidation = passwordRegex.Match(password);
        if (!passwordValidation.Success)
        {
            errors = errors + "* Password should be 6 charachters with at least 1 letter and 1 number<br />";
        }
        else if (password != confirmPassword)
        {
            errors = errors + "* Password and confirmation do not match<br />";
        }
        if (errors.Length <= 0)
        {
            if (Users.registerNewUser(email, firstName, lastname, password))
            {
                Session["userID"] = Users.getUserByEmail(email).getID();
                Session["fullName"] = Users.getUserByEmail(email).getFullName();
                Response.Redirect("dashboard.aspx");
            }
            else
            {
                RegistrationErrors = "This email Already Exist! Try Logging in";
                failedRegistration.Visible = true;
            }
        }
        else
        {
            RegistrationErrors = errors;
            failedRegistration.Visible = true;
        }
    }


    public string getRegistrationErrors()
    {
        return RegistrationErrors;
    }

    protected void FacebookUser(object sender, EventArgs e)
    {
        string email = fbemail.Value;
        string firstName = fbfirstname.Value;
        string lastname = fblastname.Value;
        string password = fbpassword.Value;
        string fbid = fbpassword.Value;

        if (Users.validateFB(fbid))
        {
            User currentUser = Users.getUserByEmail(email);
            Session["userID"] = currentUser.getID();
            Session["fullName"] = currentUser.getFullName();
            Response.Redirect("dashboard.aspx");

        }
        else if (Users.registerNewFBUser(email, firstName, lastname, password, fbid))
        {
            Session["userID"] = Users.getUserByEmail(email).getID();
            Response.Redirect("dashboard.aspx");
        }
        else
        {
            Response.Redirect("myBoards.aspx");
        }
    }

    protected void FacebookUserBoard(object sender, EventArgs e)
    {
        string email = fbemail.Value;
        string firstName = fbfirstname.Value;
        string lastname = fblastname.Value;
        string password = fbpassword.Value;
        string fbid = fbpassword.Value;

        if (Users.validateLogin(email, password))
        {
            User currentUser = Users.getUserByEmail(email);
            Session["userID"] = currentUser.getID();
            Session["fullName"] = currentUser.getFullName();
            Response.Redirect("myBoards.aspx");

        }
        else if (Users.registerNewFBUser(email, firstName, lastname, password, fbid))
        {
            Session["userID"] = Users.getUserByEmail(email).getID();
            Response.Redirect("myBoards.aspx");
        }
        else
        {
            Response.Redirect("myBoards.aspx");
        }
    }
    

}