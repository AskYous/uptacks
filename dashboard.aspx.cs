﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class dashboard : System.Web.UI.Page
{
    protected User currentUser;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userID"] != null)
        {
            currentUser = Users.getUserByID(Session["userID"].ToString());
        }
        
    }

    protected void commentButton_Click(object sender, EventArgs e)
    {

    }
    protected void retack_Click(object sender, EventArgs e)
    {
        string id = retackID.Value;
        Tack tackToRetack = Tacks.getTackById(id);
        Tacks.createNewTack(Session["userID"].ToString(), tackToRetack.getTitle(), tackToRetack.getDescription(), tackToRetack.getImgURL(), toBoardID.Value , tackToRetack.getWebsiteURL());
    }

    protected void favorite_Click(object sender, EventArgs e)
    {
        string favid = favoriteid.Value;
        Tack favorite = Tacks.getTackById(favid);
//        User current = Users.getUserByID(Session["userID"].ToString());
        favorite.favorite(currentUser,favorite);
    }
    protected void submit(object sender, EventArgs e)
    {

        Comment.insertComment(Session["userID"].ToString(), commentTackId.Value, Request.Form["commentText"],Session["fullName"].ToString());
    }

}