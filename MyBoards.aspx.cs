﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MyBoards : System.Web.UI.Page
{
    public string imgURL;
    public string directory;
    public string title;
    public string filename;
    protected void Page_Load(object sender, EventArgs e)
    {
        boardError.Visible = false;
        errorDeletingBoard.Visible = false;
//        Image1.ImageUrl = "/img/" + directory + "/" + title + "/" + filename;
    }

    public void createNewBoard(object sender, EventArgs e)
    {
        title = boardTitle.Text;
        string description = boardDescription.Text;
        string privacy = "1";
        imgURL = "";
        string errors = "";

        Regex titleRegex = new Regex(@"^[A-Za-z0-9 ]{1,}$");
        Match titleValidation = titleRegex.Match(title);
        if (!titleValidation.Success)
        {
            errors = errors + "* Title should consist of letters and numbers only<br />";
        }
        if (title.Length > 12)
        {
            errors = errors + "* Title should be 12 charecters long<br />";
        }
        if (description == "")
        {
            errors = errors + "* Description field is required<br />";
        }
        //cuz mohammed needs to complete the regex validation! :)
        if (imgURL == "" && boardImg.FileName == "")
        {
            errors = errors + "Image URL field is required<br/>";
        }

        if (errors == "")
        {
            if (isPrivate.Value.Equals("true"))
            {
                privacy = "0";
            }

            if (boardImg.PostedFile.ContentType.Contains("image"))
            {
                filename = Path.GetFileName(boardImg.FileName);

                directory = Session["userID"].ToString();

                string targetPath = Server.MapPath("~/img/") + directory; //with complete path
                bool isExists = System.IO.Directory.Exists(targetPath);
                string bp = "";
                if (!isExists)
                {
                    System.IO.Directory.CreateDirectory(targetPath);
                    bp = Server.MapPath("~/img/") + directory + "/" + title; //with complete path
                    System.IO.Directory.CreateDirectory(bp);
                    boardImg.SaveAs(bp + "/" + filename);
                    imgURL = "/img/" + directory + "/" + title + "/" + filename;
                    Image1.ImageUrl = "/img/" + directory + "/" + title + "/" + filename;
                    //name.ImageUrl="";
                    //                    boardImg.SaveAs(targetPath+"/" + filename);
                    //("~/") + imgURL
                }
                else
                {
                    bp = Server.MapPath("~/img/") + directory + "/" + title; //with complete path
                    bool isExist = System.IO.Directory.Exists(bp);
                    if (!isExist)
                    {
                        System.IO.Directory.CreateDirectory(bp);
                        boardImg.SaveAs(bp + "/" + filename);
                        imgURL = "/img/" + directory + "/" + title + "/" + filename;
                        Image1.ImageUrl = "/img/" + directory + "/" + title + "/" + filename;
                    }
                    else
                    {
                        boardImg.SaveAs(bp + "/" + filename);
                        imgURL = "/img/" + directory + "/" + title + "/" + filename;
                        Image1.ImageUrl = "/img/" + directory + "/" + title + "/" + filename;
                    }
                }
            }
        }
        
        string userID = Session["userID"].ToString();
        User currentUser = Users.getUserByID(userID);

        if (errors != "")
        {
            boardError.Text = errors;
            boardError.Visible = true;
            return;
        }
        else if (Boards.createNewBoard(userID, title, description, privacy, imgURL))
        {
            string boardID = Boards.getBoardByOwnerAndTitle(currentUser, title).getID();
            Response.Redirect("board.aspx?id=" + boardID);
        }
        else
        {
            boardError.Text = "You already have a board with that name!";
            boardError.Visible = true;
        }
    }

    protected void deleteBoard(object sender, EventArgs e)
    {
        string boardIdToDelete = deleteBoardId.Value;
        User owner = Users.getUserByID(Session["userID"].ToString());
        if (Boards.getBoardById(boardIdToDelete) != null)
        {
            Boards.deleteBoardById(boardIdToDelete);
        }
        else
        {
            errorDeletingBoard.Visible = true;
        }
    }
    protected void editBoard(object sender, EventArgs e)
    {
        Board boardToEdit = Boards.getBoardById(editBoardId.Value);

        string newBoardTitle = editBoardTitle.Value;
        string newBoardDescription = editBoardDescription.Value;
        string newBoardImgURL = editBoardImgURL.Value;
        bool newBoardIsPrivate = false;
        if (isPrivate.Value.Equals("true"))
        {
            newBoardIsPrivate = true;
        }

        boardToEdit.setTitle(newBoardTitle);
        boardToEdit.setDescription(newBoardDescription);
        boardToEdit.setImgURL(newBoardImgURL);
        boardToEdit.setPrivate(newBoardIsPrivate);
        boardToEdit.commitChanges();
    }
    protected void viewBoard(object sender, EventArgs e)
    {
        Response.Redirect("board.aspx?id=" + viewBoardId.Value);
    }
}